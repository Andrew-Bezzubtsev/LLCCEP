#include <cstdio>
#include <LLCCEP/codeReader.hpp>
#include <LLCCEP/STDExtras.hpp>

#include "jit-compiler/jit-compiler.hpp"

int main(int argn, char **argv)
{
	try {
		LLCCEP_tools::commandLineParametersParser clpp;
		clpp.addFlag(LLCCEP_tools::commandLineFlag{{"-h", "--help"},
		                                           "help", false});
		clpp.setHelpText("LLCCEP JIT Compiler help.\n"
		                 "-h/--help -- show help(this text)\n"
				 "The single free argument will be input program");
		clpp.setMaxFreeParams(1);
		clpp.parse(argn, argv);

		if (clpp.getParam("help") == "1" ||
		    !clpp.getFreeParams().size()) {
			if (!clpp.getFreeParams().size())
				::std::fprintf(stderr, "No input!\n");

			clpp.showHelp();
			return 0;
		}

		::std::FILE *in;
		OPEN_FILE(in, clpp.getFreeParams()[0].c_str(), "r");

		LLCCEP::codeReader cr(in);
		LLCCEP_JIT::jitCompiler jc;

		jc.setCodeReader(&cr);
		jc.generateCode();

		jc.exportBackendModules();
		jc.run();
	} DEFAULT_HANDLING

	return 0;
}
