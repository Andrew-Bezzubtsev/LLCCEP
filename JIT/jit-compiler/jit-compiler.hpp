#ifndef LLCCEP_JIT_JIT_COMPILER_HPP
#define LLCCEP_JIT_JIT_COMPILER_HPP

#include <llvm/ADT/iterator_range.h>
#include <llvm/ADT/STLExtras.h>
#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/ExecutionEngine/Orc/JITSymbol.h>
#include <llvm/ExecutionEngine/Orc/CompileUtils.h>
#include <llvm/ExecutionEngine/Orc/IRCompileLayer.h>
#include <llvm/ExecutionEngine/Orc/ObjectLinkingLayer.h>
#include <llvm/IR/DataLayout.h>
#include <llvm/IR/Mangler.h>

#include <algorithm>
#include <memory>
#include <string>

#include "../../native-backend/backend/backend.hpp"

namespace LLCCEP_JIT {
	typedef llvm::orc::ObjectLinkingLayer<> objectLinkLayer;
	typedef llvm::orc::IRCompileLayer<objectLinkLayer> compileLayer;
	typedef compileLayer::ModuleSetHandleT moduleHandle;
 
	class jitCompiler: public LLCCEP::nativeBackend	{
		UNCOPIABLE_CLASS(jitCompiler);

		::std::unique_ptr<llvm::TargetMachine> tm;
		llvm::DataLayout dl;
		objectLinkLayer objll;
		compileLayer cl;

		::std::vector<moduleHandle> moduleHandles;

	public:
		jitCompiler();
		~jitCompiler();

		moduleHandle addModule(::std::unique_ptr<llvm::Module> mod);
		void removeModule(moduleHandle handle);
		llvm::orc::JITSymbol findSymbol(::std::string name);
		llvm::TargetMachine &getTargetMachine() const;

		void exportBackendModules();
		void run();

	private:
		::std::string mangleOf(const ::std::string &name);
		
		template<typename T>
		static ::std::vector<T> singletonSet(T val)
		{
			::std::vector<T> res;
			res.push_back(::std::move(val));
			return res;
		}

		llvm::orc::JITSymbol findMangledSymbol(const ::std::string &name);
	};
}

#endif /* LLCCEP_JIT_JIT_COMPILER_HPP */
