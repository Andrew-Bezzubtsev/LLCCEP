#include <llvm/ExecutionEngine/ExecutionEngine.h>
#include <llvm/ExecutionEngine/RTDyldMemoryManager.h>
#include <llvm/ExecutionEngine/Orc/CompileUtils.h>
#include <llvm/ExecutionEngine/Orc/IRCompileLayer.h>
#include <llvm/ExecutionEngine/Orc/LambdaResolver.h>
#include <llvm/ExecutionEngine/Orc/ObjectLinkingLayer.h>
#include <llvm/IR/Mangler.h>
#include <llvm/Support/DynamicLibrary.h>

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/assert.hpp>

#include "jit-compiler.hpp"

LLCCEP_JIT::jitCompiler::jitCompiler():
	nativeBackend("jitCompilerModule"),
	tm(llvm::EngineBuilder().selectTarget()),
	dl(tm->createDataLayout()),
	objll(),
	cl(objll, llvm::orc::SimpleCompiler(*tm))
{
	llvm::sys::DynamicLibrary::LoadLibraryPermanently(nullptr);
}

LLCCEP_JIT::jitCompiler::~jitCompiler()
{ }

LLCCEP_JIT::moduleHandle LLCCEP_JIT::jitCompiler::addModule(std::unique_ptr<llvm::Module> mod)
{
	auto resolver = llvm::orc::createLambdaResolver(
		[&](const ::std::string &name) {
			if (auto sym = findMangledSymbol(name))
				return sym;
						            
			return llvm::orc::JITSymbol(nullptr);
		},

		[&](const ::std::string &s) {
			return llvm::orc::JITSymbol(nullptr);
		});
        
	auto handle = cl.addModuleSet(singletonSet(::std::move(mod)),
			llvm::make_unique<llvm::SectionMemoryManager>(),
			::std::move(resolver));
	
	moduleHandles.push_back(handle);
	return handle;
}

void LLCCEP_JIT::jitCompiler::removeModule(LLCCEP_JIT::moduleHandle handle)
{
	auto ref = ::std::find(moduleHandles.begin(), moduleHandles.end(), handle);
	if (ref == moduleHandles.end()) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Attempt of deletion non-attached module"));
	}

	moduleHandles.erase(ref);
}

llvm::orc::JITSymbol LLCCEP_JIT::jitCompiler::findSymbol(::std::string name)
{
	return findMangledSymbol(mangleOf(name));
}

llvm::TargetMachine &LLCCEP_JIT::jitCompiler::getTargetMachine() const
{
	return *tm;
}

void LLCCEP_JIT::jitCompiler::exportBackendModules()
{
	addModule(::std::move(nativeBackend::currentModule));
}

void LLCCEP_JIT::jitCompiler::run()
{
	auto mainSymbol = findSymbol("main");
	LLCCEP_ASSERT(mainSymbol);

	void (*mainSubmoduleFunction)() = (void (*)())(intptr_t)mainSymbol.getAddress();
	mainSubmoduleFunction();
}

::std::string LLCCEP_JIT::jitCompiler::mangleOf(const ::std::string &name)
{
	::std::string res;

	llvm::raw_string_ostream mangleName(res);
	llvm::Mangler::getNameWithPrefix(mangleName, name, dl);

	return res;
}

llvm::orc::JITSymbol LLCCEP_JIT::jitCompiler::findMangledSymbol(const ::std::string &name)
{
	for (auto &handle: llvm::make_range(moduleHandles.rbegin(), moduleHandles.rend()))
		if (auto symbol = cl.findSymbolIn(handle, name, true))
			return symbol;

	return NULL;
}

