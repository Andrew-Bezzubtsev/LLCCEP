#include <QtGlobal>
#include <QFont>
#include <QPainter>
#include <QPaintEvent>
#include <QResizeEvent>
#include <QTimer>

#include <LLCCEP/renderer.hpp>

LLCCEP::renderer::renderer(QWidget *parent):
	QWidget(parent),
	update_timer(NULL),
	image_painter(NULL),
	framebuf(NULL),
	antialiased(false),
	started(false),
	locked(false)
{
	setBackgroundRole(QPalette::Base);
	setAutoFillBackground(true);
}

LLCCEP::renderer::~renderer()
{
	end();
	delete update_timer;
}

void LLCCEP::renderer::begin(int w, int h, int fps)
{
	framebuf = new QPixmap(w, h);
	image_painter = new QPainter(framebuf);

	image_painter->setPen(QPen(Qt::white, 1));
	image_painter->setBrush(QBrush(Qt::transparent));
	image_painter->setFont(QFont("Courier"));

	update_timer = new QTimer(this);
	connect(update_timer, SIGNAL(timeout()), this, SLOT(update()));

	started = true;
	update_timer->start(1000 / fps);
}

void LLCCEP::renderer::setAffineTransformData(double tX, double tY,
                                              double alpha,
					      double sX, double sY)
{
	Q_ASSERT(ok());

	image_painter->translate(tX, tY);
	image_painter->rotate(alpha);
	image_painter->scale(sX, sY);

	Q_ASSERT(ok());
}

void LLCCEP::renderer::setPen(const QPen &pen)
{
	Q_ASSERT(ok());

	image_painter->setPen(pen);

	Q_ASSERT(ok());
}

void LLCCEP::renderer::setBrush(const QBrush &brush)
{
	Q_ASSERT(ok());

	image_painter->setBrush(brush);

	Q_ASSERT(ok());
}

void LLCCEP::renderer::setAntialiased(bool aalias)
{
	Q_ASSERT(ok());

	antialiased = aalias;

	Q_ASSERT(ok());
}

void LLCCEP::renderer::lock()
{
	Q_ASSERT(ok());

	locked = !locked;

	Q_ASSERT(ok());
}

void LLCCEP::renderer::end()
{
	delete image_painter;
	delete framebuf;

	image_painter = NULL;
	framebuf      = NULL;
	
	started       = false;
	antialiased   = false;
	locked        = false;
}

QPainter &LLCCEP::renderer::painter() const
{
	Q_ASSERT(ok());

	return *image_painter;
}

QImage LLCCEP::renderer::getImage() const
{
	Q_ASSERT(ok());

	return framebuf->toImage();
}

bool LLCCEP::renderer::ok() const
{
	return image_painter && framebuf && started;
}

void LLCCEP::renderer::paintEvent(QPaintEvent *)
{
	QPainter painter(this);

	painter.setRenderHint(QPainter::Antialiasing, antialiased);

	if (!locked && ok())
		painter.drawPixmap(0, 0, *framebuf);

	painter.setRenderHint(QPainter::Antialiasing, false);
}

void LLCCEP::renderer::resizeEvent(QResizeEvent *event)
{
	if (!ok())
		return;

	int w0 = framebuf->size().width(), h0 = framebuf->size().height(),
	    w1 = event->size().width(), h1 = event->size().height();

	if (w1 > w0 || h1 > h0) {
		QPixmap *newPixmap = new QPixmap(::std::max(w0, w1), ::std::max(h0, h1));
		QPainter *newPainter = new QPainter(newPixmap);

		newPainter->setBackground(image_painter->background());
		newPainter->setBackgroundMode(image_painter->backgroundMode());
		newPainter->setBrush(image_painter->brush());
		newPainter->setBrushOrigin(image_painter->brushOrigin());
		newPainter->setClipPath(image_painter->clipPath());
		newPainter->setClipRect(image_painter->clipBoundingRect());
		newPainter->setClipRegion(image_painter->clipRegion());
		newPainter->setClipping(image_painter->hasClipping());
		newPainter->setCompositionMode(image_painter->compositionMode());
		newPainter->setFont(image_painter->font());
		newPainter->setLayoutDirection(image_painter->layoutDirection());
		newPainter->setMatrix(image_painter->matrix());
		newPainter->setMatrixEnabled(image_painter->matrixEnabled());
		newPainter->setOpacity(image_painter->opacity());
		newPainter->setPen(image_painter->pen());
		newPainter->setRenderHints(image_painter->renderHints());
		newPainter->setTransform(image_painter->transform());
		newPainter->setViewport(image_painter->viewport());
		newPainter->setViewTransformEnabled(image_painter->viewTransformEnabled());
		newPainter->setWindow(image_painter->window());
		newPainter->setWorldMatrix(image_painter->worldMatrix());
		newPainter->setWorldMatrixEnabled(image_painter->worldMatrixEnabled());
		newPainter->setWorldTransform(image_painter->worldTransform());

		newPainter->drawPixmap(0, 0, *framebuf);

		delete image_painter;
		delete framebuf;

		image_painter = newPainter;
		framebuf = newPixmap;
	}

	Q_ASSERT(OK());
}
