#include <string>

#include <LLCCEP/basicBlock.hpp>

LLCCEP::basicBlock::basicBlock():
	position(0),
	blockName(::std::string("%%") + to_string(anonCounter))
{
	anonCounter++;
}

LLCCEP::basicBlock::basicBlock(::std::string name, size_t pos):
	position(pos),
	blockName(name)
{ }

LLCCEP::basicBlock::basicBlock(size_t pos):
	position(pos),
	blockName(::std::string("%%") + to_string(anonCounter))
{
	anonCounter++;
}

LLCCEP::basicBlock::~basicBlock()
{ }

::std::string LLCCEP::basicBlock::name() const
{
	return blockName;
}

void LLCCEP::basicBlock::name(::std::string newName)
{
	if (!newName.length()) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Empty name is denied to basicBlock name"));
	}

	blockName = newName;
}

size_t LLCCEP::basicBlock::address() const
{
	return position;
}

void LLCCEP::basicBlock::address(size_t newAddress)
{
	position = newAddress;
}
