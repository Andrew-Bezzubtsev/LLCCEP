#include <vector>
#include <string>
#include <cstdio>

#include <LLCCEP/basicBlock.hpp>
#include <LLCCEP/variable.hpp>
#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/module.hpp>

#define MODULE_CHECK_BLOCK(cond) DEFAULT_CHECK_BLOCK(cond, this, ok());
#define MODULE_OK MODULE_CHECK_BLOCK(true)
#define MODULE_NOT_OK MODULE_CHECK_BLOCK(false)

LLCCEP::module::module():
	output(NULL),
	name(),
	filename(),
	bb(),
	variables()
{ }

LLCCEP::module::~module()
{ }

void LLCCEP::module::outputFile(::std::FILE *newOutput)
{
	if (!newOutput) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid output to module dumper"));
	}

	output = newOutput;
}

void LLCCEP::module::outputFilename(::std::string newFilename)
{
	if (!newFilename.length()) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Empty output filename"));
	}

	filename = newFilename;
}

void LLCCEP::module::moduleName(::std::string name)
{
	if (!name.length()) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Empty module name!\n"));
	}

	name = newName;
}

::std::FILE *LLCCEP::module::outputFile() const
{
	MODULE_OK;

	return output;
}

::std::string LLCCEP::module::moduleFilename() const
{
	MODULE_OK;
	
	return filename;
}

::std::string LLCCEP::module::moduleName() const
{
	MODULE_OK;

	return name;
}

void LLCCEP::module::dump() const
{
	MODULE_OK;

	::std::fprintf(stderr, "LLCCEP code generation module dump\n"
	                       "Name: %s\n"
			       "Output filename: %s\n"
			       "Registered variables list: ",
			       name.c_str(), filename.c_str());

	for (auto i = variables.begin(); i != variables.end(); i++) {
		::std::fputs(i->name().c_str(), stderr);

		if (i != variables.end() - 1)
			::std::fputs(", ", stderr);
	}

	::std::fputc('\n', stderr);
	::std::fputs("Basic blocks list: ", stderr);

	for (auto i = bb.begin(); i != bb.end(); i++) {
		::std::fputs(i->name().c_str(), stderr);

		if (i != variables.end() - 1)
			::std::fputs(", ", stderr);
	}

	::std::fputc('\n', stderr);

	MODULE_OK;
}

bool LLCCEP::module::ok() const
{
	return name.length() && filename.length() && output;
}

#undef MODULE_CHECK_BLOCK
#undef MODULE_OK
#undef MODULE_NOT_OK
