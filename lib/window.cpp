#include <LLCCEP/window.hpp>

LLCCEP::window::window():
	renderer(),
	kb({}),
	mouse(),
	mouse_buttons(0),
	may_close(false)
{ }

bool LLCCEP::window::getKBButtonState(uint8_t id) const
{
	return kb[id];
}

QPoint LLCCEP::window::getMousePos() const
{
	return mouse;
}

int LLCCEP::window::getMouseButtons() const
{
	return mouse_buttons;
}

void LLCCEP::window::setMayClose(bool newMayClose)
{
	may_close = newMayClose;
}

bool LLCCEP::window::mayClose() const
{
	return may_close;
}

void LLCCEP::window::closeEvent(QCloseEvent *event)
{
#define KILL() \
{ \
	end(); \
	event->accept(); \
}

	if (may_close) {
		KILL();
	} else {
		int res = LLCCEP::messageBox("Program is still executing!\n"
		                             "Do you really want to terminate it?",
					     QMessageBox::No | QMessageBox::Yes,
					     QMessageBox::No, QMessageBox::Warning).spawn();

		if (res == QMessageBox::Yes)
			KILL();
		else
			event->ignore();
	}
#undef KILL
}

void LLCCEP::window::keyEvent(QKeyEvent *event)
{
	size_t key = event->key();
	if (key <= 0xFF)
		kb[key] = !kb[key];
}

void LLCCEP::window::mouseEvent(QMouseEvent *event)
{
	mouse = event->pos();
	mouse_buttons = (int)event->buttons();
}
