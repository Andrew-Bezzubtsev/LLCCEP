#include <cstdio>
#include <string>

#include <LLCCEP/convert.hpp>
#include <LLCCEP/value.hpp>
#include <LLCCEP/STDExtras.hpp>

LLCCEP::value::value()
{ }

LLCCEP::value::~value()
{ }

LLCCEP::registerValue::registerValue():
	regId(SIZE_MAX)
{ }

LLCCEP::registerValue::registerValue(size_t id):
	regId(id)
{
	check();
}

size_t LLCCEP::registerValue::id() const
{
	check();
	return regId;
}

void LLCCEP::registerValue::id(size_t newId) const
{
	regId = newId;
	check();
}

::std::string LLCCEP::registerValue::getMnemonic() const
{
	check();

	::std::string res = "&";
	res += to_string(regId);

	return res;
}

void LLCCEP::registerValue::dump() const
{
	check();

	::std::fprintf(stderr, "Register value(&" size_t_pf " register)\n", regId);
}

void LLCCEP::registerValue::check() const
{
	if (regId > 31) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid register id. [0, 31] is required"));
	}
}

LLCCEP::ramValue::ramValue():
	vaddress(0)
{ }

LLCCEP::ramValue::ramValue(size_t addr):
	vaddress(addr)
{ }

size_t LLCCEP::ramValue::vaddr() const
{
	return vaddress;
}

void LLCCEP::ramValue::vaddr(size_t newVaddr) const
{
	vaddress = newVaddr;
}

::std::string LLCCEP::ramValue::getMnemonic() const 
{
	::std::string res = "$";
	res += to_string(regId);
	
	return res;
}

void LLCCEP::ramValue::dump() const
{
	::std::fprintf(stderr, "Ram value($" size_t_pf " cell)\n", vaddress);
}

LLCCEP::constantValue::constantValue():
	val(0)
{ }

LLCCEP::constantValue::constantValue(double v):
	val(v)
{ }

double LLCCEP::constantValue::value() const
{
	return val;
}

void LLCCEP::constantValue::value(double v) const
{
	val = v;
}

::std::string LLCCEP::constantValue::getMnemonic() const
{
	return to_string(val);
}

void LLCCEP::constantValue::dump() const
{
	::std::fprintf(stderr, "Constant FP value(%lg)\n", val);
}
