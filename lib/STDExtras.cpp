#ifdef UNICODE
#undef UNICODE
#endif

#ifdef _UNICODE
#undef _UNICODE
#endif

#if defined(_WIN32)
#include <windows.h>
#elif defined(__linux__) || defined(__MACH__)
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

/* To provide access to
   strerror_s and
   strerrorlen_s functions */
#define __STDC_WANT_LIB_EXT1__ 1

#include <cstdio>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <cassert>
#include <stdexcept>
#include <sstream>
#include <vector>
#include <string>
#include <regex>

#if defined(__linux__)
#include <linux/limits.h>
#endif // linux

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/os-specific.hpp>
#include <LLCCEP/command-line.hpp>

#define MAKE_EXCEPTION_MESSAGE(file, line, func, msg, cause)\
({ \
	char *__res = new char[MAX_EXC_BUF_SIZE]; \
	::std::sprintf(__res, "A runtime exception was caught!\n" \
		              "File: %s\n" \
			      "Function subscript: %s\n" \
			      "Line: " size_t_pf "\n" \
			      "Message: '%s'\n", \
	                      file, func, line, msg); \
	if (cause) \
		::std::sprintf(__res, "%sCaused by: %s", \
			              __res, cause->what());\
\
	__res; \
})

namespace LLCCEP {
	runtime_exception::runtime_exception():
		runtime_error(""),
		__text__()
	{
		FATAL_ERROR(yes, "runtime exception constructor",
   		                 "invalid initalization type:"
		                 "runtime_exception()")
	}

	runtime_exception::runtime_exception(const char file[PATH_MAX],
	                                     size_t line,
	                                     const char function[512],
										 const char *msg,
										 bool freemsg,
	                                     runtime_exception *cause):
		runtime_error(msg),
		__text__()
	{
		char *data = MAKE_EXCEPTION_MESSAGE(file, line, function,
		                                    msg, cause);

		strcpy(__text__, data);
		free((void *)data);

		if (freemsg)
			free((void *)msg);
	}

	runtime_exception::~runtime_exception() throw()
	{ }

	const char *runtime_exception::what() const throw()
	{
		return __text__;
	}

	const char *runtime_exception::msg() const throw()
	{
		return runtime_error::what();	
	}
}

::std::string std::extras::strerror_safe(int errid)
{
	::std::string res;

#if defined(_WIN32)
	size_t len = strerrorlen_s(errid) + 1;
	char *buffer = reinterpret_cast<char *>(::std::malloc(len + 1));

	strerror_s(buffer, len, errid);
	res = ::std::string(buffer);
	::std::free(reinterpret_cast<void *>(buuffer));
#elif defined(__MACH__) || defined(__linux__)
	res = strerror(errid);
#else
#error Unknown OS
#endif

	return res;
}

size_t std::extras::flen(::std::FILE *in)
{
	if (!in) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid file to get length"));
	}

	::std::fseek(in, 0, SEEK_END);
	size_t res = ::std::ftell(in);
	::std::rewind(in);

	return res;
}

::std::string std::fix::getline(::std::FILE *in)
{
	if (!in) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid file pointer to getline"));
	}

	::std::string res = "";
	char c = 0;
	while (!::std::feof(in)) {
		c = ::std::fix::fgetc(in);
		if (c && c != '\n')
			res += c;
		else
			break;
	}

	return res;
}

char std::fix::fgetc(::std::FILE *in)
{
	if (::std::feof(in)) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Attempt of reading char from empty file"));
	}

	return ::std::fgetc(in);
}

size_t std::fix::fread(void *mem, size_t sz, size_t amount, FILE *in)
{
	for (size_t i = 0; i < amount; i++) {
		for (size_t j = 0; j < sz; j++) {
			*(reinterpret_cast<char *>(mem) + i * sz + j) = ::std::fix::fgetc(in);
		}
	}

	return sz * amount;
}

bool filesystem::fileExists(::std::string relpath)
{
#if defined(__linux__) || defined(__MACH__)
	struct stat buf;
	return !stat(relpath.c_str(), &buf);
#elif defined(_WIN32) || defined(_WIN64)
	WIN32_FIND_DATA findData;
	HANDLE handle = FindFirstFile(relpath.c_str(), 
	                              &findData);
	bool res = handle != INVALID_HANDLE_VALUE;
	
	if (res)
		FindClose(handle);
	return res;
#else
#error Undefined OS
#endif
}

::std::string filesystem::getAbsolutePath(::std::string relpath)
{
	if (!filesystem::fileExists(relpath))
		return relpath;

	::std::string res;

#if defined(__linux__) || defined(__MACH__)
	char *fullpath = realpath(relpath.c_str(), 0);
	res = fullpath;
	delete fullpath;
#elif defined(_WIN32)
	char fullpath[PATH_MAX] = "";
	GetFullPathName(relpath.c_str(), MAX_PATH, fullpath, 0);
	res = fullpath;
#else
#error Undefined OS.
#endif
	
	return res;
}

::std::vector<::std::string> tokenizer::split(::std::string str,
											  ::std::string delimiters,
											  ::std::string allowed)
{
	char *inputString = reinterpret_cast<char *>(::std::malloc(str.length() + 1));
	::std::strcpy(inputString, str.c_str());

	::std::vector<::std::string> res;
	::std::regex allowedRegex(allowed, ::std::regex_constants::egrep);
	char *pch = ::std::strtok(inputString, delimiters.c_str());

	while (pch) {
		if (!::std::regex_match(pch, allowedRegex)) {
			throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
				"Forbidden character in '%s' string",
				pch));
		}

		res.push_back(pch);
		pch = ::std::strtok(NULL, delimiters.c_str());
	}

	::std::free(reinterpret_cast<void *>(inputString));

	return res;
}

void dump_bytes(::std::FILE *out, ::std::vector<uint8_t> list)
{
	for (const auto &i: list)
		fputc(i, out);
}

#undef MAKE_EXCEPTION_MESSAGE
