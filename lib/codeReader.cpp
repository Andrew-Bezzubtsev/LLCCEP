#include <vector>
#include <cstring>
#include <cerrno>
#include <cstdio>

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/STLExtras.hpp>
#include <LLCCEP/convert.hpp>
#include <LLCCEP/os-specific.hpp>
#include <LLCCEP/codeReader.hpp>

#include "./../assembler/lexer/lexer.hpp"
#include "./../common/def/def_inst.hpp"

#define CHECK_PROGRAM_READER(cond) DEFAULT_CHECK_BLOCK(cond, this, OK());

#define PROGRAM_READER_OK CHECK_PROGRAM_READER(true)
#define PROGRAM_READER_NOTOK CHECK_PROGRAM_READER(false)

LLCCEP::codeReader::codeReader():
	input(NULL),
	data()
{ }

LLCCEP::codeReader::codeReader(::std::FILE *in):
	input(NULL),
	data()
{
	/* Initialize input file */
	initializeInputFile(in);
}

void LLCCEP::codeReader::initializeInputFile(::std::FILE *in)
{
	PROGRAM_READER_NOTOK;

	/* If input is invalid/failed */
	if (!in) {
		/* Throw exception about it */
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid input to codeReader"));
	}

	/* If the input is OK, make it the new input */
	input = in;

	PROGRAM_READER_OK;
}

void LLCCEP::codeReader::readProgramHeader()
{
	PROGRAM_READER_OK;

	uint8_t sz = 0;
	size_t off = 0;

	/* Go to beginning of the file */
	rewind(input);

	/* Get debug symbols info */
	data.debug = std::fix::fgetc(input);
	/* Get word size on machine, built the
	 * program
	 */
	sz = std::fix::fgetc(input);
	data.wordSize = sz;

	/* If word size differes, so that
	 * loading data from the source can
	 * cause data loss
	 */
	if (sz > sizeof(size_t)) {
		/* Throw exception about it with
		 * comprehensive information
		 */
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Can't execute the program:\n"
			"The word length on machine, compiled\n"
			"the program is greater than on this one.\n"
			"(" size_t_pf "-bit machine required)",
			static_cast<size_t>(sz)));
	}

	/* Read the ID of main label */
	std::fix::fread(reinterpret_cast<char *>(&data.main_id), data.wordSize, input);

	/* If there are debug symbols in
	 * the file
	 */
	if (data.debug) {
		/* Read amount of files,
		 * used to build the program
		 */
		std::fix::fread(reinterpret_cast<char *>(&data.filesno), data.wordSize, input);

		/* Do for the number of times,
		 * equal to the amount of files
		 * used in program compilation */
		for (size_t i = 0; i < data.filesno; i++) {
			/* Filename */
			::std::string fname;
			/* The first produced from the
			 * file instruction id
			 */
			size_t begin;
			/* Temporary character */
			char c;

			/* While c is not Null-character */
			while ((c = ::std::fix::fgetc(input))) {
				/* Emit the characted to filename */
				fname += c;
			}

			/* Save filename */
			data.fnames.push_back(fname);

			/* Calculate new header size
			 * (offset to program)
			 */
			off += fname.length() + 2 * sz + 1;
			/* Get the first produced from the file
			 * instruction id
			 */
			::std::fix::fread(reinterpret_cast<char *>(&begin), data.wordSize, input);

			/* Save the first produced from the
			 * file instruction id
			 */
			data.beginnings.push_back(begin);
		}
	}

	/* Get program length */
	size_t pl = ::std::extras::flen(input) - 2 - off - sz;
	/* Get length of instruction */
	size_t len = data.debug?
				 LLCCEP::DINSTRUCTION_LENGTH:
				 LLCCEP::INSTRUCTION_LENGTH;

	/* If there are no just instructions after header,
	 * but a couple of junk bytes
	 */
	if (pl % len) {
		/* Throw exception about it */
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Can't execute the program:\n"
			"Invalid or damaged binary file!\n"
			"Namely, the length of input, "
			"excluding header, not product of " size_t_pf ",\n"
			"which is instruction length.",
			len));
	}

	/* Store program size
	 * in instructions
	 */
	data.size = pl / len;

	/* Store offset to program
	 * beginning(just size
	 * of header in bytes)
	 */
	data.offset = off + sz + 2;

	PROGRAM_READER_OK;
}

LLCCEP::instruction LLCCEP::codeReader::getInstruction(size_t id)
{
	PROGRAM_READER_OK;
		
	/* If identifier of instruction to get
	 * is out of program's bounds.
	 * (It may occur after missing the EOF)
	 */
	if (id >= data.size) {
		/* Return reserved instruction with
		 * no arguments, requiring
		 * program termination
		 */
		return LLCCEP::instruction{
			UINT8_MAX,
			{{LLCCEP_ASM::LEX_T_NO, 0},
			 {LLCCEP_ASM::LEX_T_NO, 0},
			 {LLCCEP_ASM::LEX_T_NO, 0}}};
	}

	/* Get instruction info length */
	size_t len = data.debug?
	             LLCCEP::DINSTRUCTION_LENGTH:
		     LLCCEP::INSTRUCTION_LENGTH;
	/* Set pointer to instruction's beginning and
	 * skip, if needed, the line number, where it
	 * was presented(if there are debug symbols)
	 */
	::std::fseek(input, data.offset + id * len + (data.debug?data.wordSize:0), SEEK_SET);

	/* Result */
	LLCCEP::instruction res{};
	/* Read instruction opcode */
	res.opcode = ::std::fix::fgetc(input);
	/* Get 3 arguments from file */
	for (unsigned i = 0; i < LLCCEP_ASM::MAX_ARGNO; i++) {
		/* Get argument type */
		res.args[i].type = static_cast<LLCCEP_ASM::lex_t>(::std::fix::fgetc(input));
		/* Get argument addition value */
		::std::fix::fread(reinterpret_cast<char *>(&(res.args[i].val)), sizeof(double), input);
	}

	/* If the opcode of
	 * instruction is overbounded
	 */
	if (res.opcode >= LLCCEP_ASM::INST_NUM) {
		/* Throw exception about it */
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Opcode is overbounded!\n"));
	}

	PROGRAM_READER_OK;

	/* Return read instruction */
	return res;
}

LLCCEP::codeData LLCCEP::codeReader::getProgramData() const
{
	PROGRAM_READER_OK;

	/* Return program's header info */
	return data;
}

::std::string LLCCEP::codeReader::getInstructionFile(size_t id)
{
	PROGRAM_READER_OK;

	/* Process all files beginnings in
	 * executable code
	 */
	for (size_t i = 0; i < data.beginnings.size(); i++) {
		/* If instruction id is greater or equal to the beginning of file
		 * and is above the beginning of next file */
		if (id >= data.beginnings[i] && i < data.fnames.size() &&
			(i < data.beginnings.size() - 1)?
		    (id < data.beginnings[i + 1]):
		    (true)) {
			/* The file wanted*/
			return data.fnames[i];
		}
	}

	throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
		"Can't define instruction's file"));

	PROGRAM_READER_OK;
}

size_t LLCCEP::codeReader::getFileBeginning(::std::string fname)
{
	PROGRAM_READER_OK;

	size_t id = vec_find(data.fnames, fname) - data.fnames.begin();
	if (id >= data.fnames.size() || id >= data.beginnings.size()) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid filename"))
	}

	return data.beginnings[id];

	PROGRAM_READER_OK;
}

size_t LLCCEP::codeReader::getFileEnding(::std::string fname)
{
	PROGRAM_READER_OK;

	size_t id = vec_find(data.fnames, fname) - data.fnames.begin();
	if (id >= data.fnames.size() || id >= data.beginnings.size()) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid filename"));
	}

	return ((id == data.beginnings.size() - 1)?
		(data.size - 1):
		(data.beginnings[id + 1] - 1));

	PROGRAM_READER_OK;
}

size_t LLCCEP::codeReader::getInstructionLine(size_t id)
{
	PROGRAM_READER_OK

	size_t res = 0;
	size_t len = data.debug?
	             LLCCEP::DINSTRUCTION_LENGTH:
		     LLCCEP::INSTRUCTION_LENGTH;
	if (id >= data.size) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Can't get line of invalid instruction"));
	}

	::std::fseek(input, data.offset + id * len, SEEK_SET);
	::std::fix::fread(reinterpret_cast<char *>(&res), data.wordSize, input);

	PROGRAM_READER_OK

	return res;
}

bool LLCCEP::codeReader::forDebugRun()
{
	PROGRAM_READER_OK;

	return static_cast<bool>(data.debug);
}

bool LLCCEP::codeReader::OK() const
{
	return input && !(::std::ferror(input));
}

::std::string LLCCEP::getArgMnemonic(LLCCEP::arg argument)
{
	::std::stringstream out;

	switch (argument.type) {
	case LLCCEP_ASM::LEX_T_REG:
		out << "&" << static_cast<size_t>(argument.val);
		break;

	case LLCCEP_ASM::LEX_T_MEM:
		out << "$" << static_cast<size_t>(argument.val);
		break;

	case LLCCEP_ASM::LEX_T_VAL:
		out << argument.val;
		break;

	case LLCCEP_ASM::LEX_T_COND:
		out << "@" << LLCCEP_ASM::get_condition_mnemonic(static_cast<uint8_t>(argument.val));
		
		break;

	case LLCCEP_ASM::LEX_T_NO:
		break;

	default:
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Non-argument to be dumped"));
	}

	return out.str();
}

::std::string LLCCEP::getInstructionMnemonic(LLCCEP::instruction inst)
{
	::std::stringstream out;

	for (unsigned i = 0; i < LLCCEP_ASM::MAX_ARGNO; i++) {
		if (inst.args[i].type == LLCCEP_ASM::LEX_T_NO)
			break;

		if (i)
			out << ", ";
			
		out << LLCCEP::getArgMnemonic(inst.args[i]);
	}

	return out.str();
}

#undef CHECK_PROGRAM_READER
#undef PROGRAM_READER_OK
#undef PROGRAM_READER_NOTOK
