#include <string>
#include <vector>
#include <map>
#include <cstdio>

#include <LLCCEP/basicBlock.hpp>
#include <LLCCEP/value.hpp>
#include <LLCCEP/IR.hpp>

#define IR_GENERATOR_CHECK(cond) DEFAULT_CHECK_BLOCK(cond, this, ok())
#define IR_GENERATOR_OK IR_GENERATOR_CHECK(true)
#define IR_GENERATOR_NOT_OK IR_GENERATOR_CHECK(false)

LLCCEP::IRGenerator::IRGenerator():
	freeptr(0),
	released(),
	vars(),
	pc(0),
	output(stdout)
{ }

LLCCEP::IRGenerator::~IRGenerator()
{ }

LLCCEP::ramValue LLCCEP::IRGenerator::createVariable(std::string varName)
{
	IR_GENERATOR_OK;

	if (vars.find(varName) != vars.end()) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"'%s' variable was already declared",
			varName.c_str()));
	}

	size_t ptr;
	if (released.size()) { 
		ptr = *(released.end() - 1);
		released.pop_back();
	} else {
		ptr = freeptr;
		freeptr++;
	}

	vars[varName] = ptr;

	IR_GENERATOR_OK;

	return LLCCEP::ramValue(vars);
}

void LLCCEP::IRGenerator::releaseVariable(::std::string varName)
{
	IR_GENERATOR_OK;

	auto f = vars.find(varName);
	if (f == vars.end()) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid '%s' variable to free",
			varName.c_str()))
	}

	released.push_back(f->second);
	vars.erase(f);

	IR_GENERATOR_OK;
}

size_t LLCCEP::IRGenerator::PC() const
{
	IR_GENERATOR_OK;

	return pc;
}

void LLCCEP::IRGenerator::insertMov(LLCCEP::value out, value in)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "mov %s, %s\n", out.getMnemonic().c_str(),
  	               in.getMnemonic().c_str());	
	
	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertMva(LLCCEP::value out, LLCCEP::value in)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "mva %s, %s\n", out.getMnemonic().c_str(),
		       in.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertPush(LLCCEP::value val)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "push %s\n", val.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertPop()
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "pop");

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertTop(value out)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "top %s\n", out.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertAdd(LLCCEP::value out, 
                                    LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "add %s, %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str(), i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertSub(LLCCEP::value out, 
                                    LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "sub %s, %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str(), i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertMul(LLCCEP::value out, 
                                    LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "mul %s, %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str(), i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertDiv(LLCCEP::value out, 
                                    LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "div %s, %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str(), i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertAnd(LLCCEP::value out, 
                                    LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "and %s, %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str(), i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertOr(LLCCEP::value out, 
                                   LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "or %s, %s, %s\n", out.getMnemonic().c_str(),
 	               i0.getMnemonic().c_str(), i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertXor(LLCCEP::value out, 
                                    LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "xor %s, %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str(), i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertOff(LLCCEP::value out, 
                                    LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "off %s, %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str(), i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertNop()
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "nop\n");

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertSwi(LLCCEP::value id)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "swi %s\n", id.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertCmp(LLCCEP::value i0, LLCCEP::value i1)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "cmp %s, %s\n", i0.getMnemonic().c_str(),
	               i1.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertInc(LLCCEP::value i0)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "inc %s\n", i0.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertDec(LLCCEP::value i0)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "dec %s\n", i0.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertSqrt(LLCCEP::value out, LLCCEP::value i0)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "sqrt %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertSin(LLCCEP::value out, LLCCEP::value i0)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "sin %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertCos(LLCCEP::value out, LLCCEP::value i0)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "cos %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertPtan(LLCCEP::value out, LLCCEP::value i0)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "ptan %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertPatan(LLCCEP::value out, LLCCEP::value i0)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "patan %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertLdc(LLCCEP::value out, LLCCEP::value i0)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "ldc %s, %s\n", out.getMnemonic().c_str(),
	               i0.getMnemonic().c_str());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertCall(LLCCEP::condition cond, LLCCEP::basicBlock bb)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "call %s, " size_t_pf "\n", 
	               cond.getMnemonic().c_str(),
   	               bb.address());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertJmp(LLCCEP::condition cond, LLCCEP::basicBlock bb)
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "jmp %s, " size_t_pf "\n", 
	               cond.getMnemonic().c_str(),
	               bb.address());

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertRet()
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "ret\n");

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertStregs()
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "stregs\n");

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::insertLdregs()
{
	IR_GENERATOR_OK;

	::std::fprintf(output, "ldregs\n");

	IR_GENERATOR_OK;
}

void LLCCEP::IRGenerator::outputFile(::std::FILE *out = stdout)
{
	if (!out) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid output file to IR generator"));
	}

	output = out;
}

bool LLCCEP::IRGenerator::ok() const
{
	return static_cast<bool>(output);
}

#undef IR_GENERATOR_CHECK
#undef IR_GENERATOR_OK
#undef IR_GENERATOR_NOT_OK
