/**
 * @file align.h
 *
 * $Copyright: (C) Andrew Bezzubtsev, 2016 $
 * 
 * @par A cross-platform compiler-independent wrapper for alignment attribute.
 * This library provides you a compiler-independent
 * macro for aligning structures, etc up to some defined size.
 */
#ifndef LLCCEP_ALIGN_H
#define LLCCEP_ALIGN_H

#if defined __DOXYGEN__
/**
 * @ingroup Workarounds
 *
 * This macro provides you making
 * alignment without worry, what compiler will
 * be used by target user to build your application
 * while aligning some data structure.
 *
 * @code
 * struct point_t {
 *     int x;
 *     int y;
 * } __align__(8);
 * @endcode
 */
#define __align__(b)
#elif defined __GNUC__ || defined __clang__
#define __align__(b) __attribute__((aligned(b)))
#elif defined _MSC_VER
#define __align__(b) __declspec(align(b))
#endif

#endif /* LLCCEP_ALIGN_H */
