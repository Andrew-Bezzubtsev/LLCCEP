#ifndef LLCCEP_MODULE_HPP
#define LLCCEP_MODULE_HPP

#include <vector>
#include <string>
#include <cstdio>

#include <LLCCEP/basicBlock.hpp>
#include <LLCCEP/variable.hpp>
#include <LLCCEP/STDExtras.hpp>

namespace LLCCEP {
	class module {
		UNCOPIABLE_CLASS(module);

		::std::FILE *output;
		::std::string name;
		::std::string filename;

		::std::vector<basicBlock> bb;
		::std::vector<variable> variables;

	public:
		module();
		~module();

		void outputFile(::std::FILE *newOutput);
		void outputFilename(::std::string newFilename);
		void moduleName(::std::string newName);

		::std::FILE *outputFile() const;
		::std::string outputFilename() const;
		::std::string moduleName() const;

		void dump() const;

	private:
		bool ok() const;
	};
}

#endif /* LLCCEP_MODULE_HPP */
