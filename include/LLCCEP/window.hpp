#ifndef LLCCEP_WINDOW_HPP
#define LLCCEP_WINDOW_HPP

#include <LLCCEP/renderer.hpp>

namespace LLCCEP {
	class window: public renderer {
		Q_OBJECT

	public:
		window();
		
		bool getKBButtonState(uint8_t id) const;
		QPoint getMousePos() const;
		int getMouseButtons() const;

		void setMayClose(bool newMayClose = true);
		bool mayClose() const;

	protected:
		virtual void closeEvent(QCloseEvent *event) Q_DECL_OVERRIDE;
		virtual void keyEvent(QKeyEvent *event);
		virtual void mouseEvent(QMouseEvent *event);

		bool kb[256];
		QPoint mouse;
		int mouse_buttons;
		bool may_close;
	};
}

#endif /* LLCCEP_WINDOW_HPP */
