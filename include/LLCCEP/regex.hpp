#ifndef LLCCEP_REGEX_HPP
#define LLCCEP_REGEX_HPP

#include <regex>

#define LLCCEP_REGEX(regexPresentation) ::std::regex(regexPresentation, ::std::regex_constants::egrep)

#endif /* LLCCEP_REGEX_HPP */
