#ifndef LLCCEP_COMPILE_TIME_HPP
#define LLCCEP_COMPILE_TIME_HPP

#define LLCCEP_FILE __FILE__
#define LLCCEP_LINE __LINE__

#if defined __GNUC__ || defined __clang__
#define LLCCEP_FUNCTION __PRETTY_FUNCTION__
#elif defined _MSC_VER
#define LLCCEP_FUNCTION __FUNCTION__
#else
#define LLCCEP_FUNCTION __FUNC__
#endif

#endif /* LLCCEP_COMPILE_TIME_HPP */
