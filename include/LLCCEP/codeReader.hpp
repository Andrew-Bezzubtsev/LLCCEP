#ifndef INCLUDE_CODEREADER_HPP
#define INCLUDE_CODEREADER_HPP

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/align.h>

#include <vector>
#include <iostream>
#include <cstdint>

#include "./../assembler/lexer/lexer.hpp"
#include "./../common/def/def_cond.hpp"
#include "./../common/def/def_inst.hpp"

namespace LLCCEP {
	/* Module's constants */
	const size_t INSTRUCTION_LENGTH  = 28;
	const size_t DINSTRUCTION_LENGTH = 36;

	struct arg {
		/* Argument type */
		LLCCEP_ASM::lex_t type;
		/* Argument value */
		double val;
	} __align__(8);

	struct instruction {
		/* Opcode */
		uint8_t opcode;
		/* Instruction arguments */
		arg args[LLCCEP_ASM::MAX_ARGNO];
	} __align__(8);

	/* Input program data:
	   - offset of program, in bytes
	   - main label position
	   - size of program, in instructions */
	struct codeData {
		/* Offset of program, in bytes */
		size_t offset;
		/* Offset of main, in instructions */
		size_t main_id;
		/* Program size, in instructions */
		size_t size;
		/* Word size */
		size_t wordSize;

		/* Is executable for debugging */
		uint8_t debug;
		/* Number of files of executable */
		size_t filesno;
		/* Beginning of code from sources */
		::std::vector<size_t> beginnings;
		/* Filenames */
		::std::vector<::std::string> fnames;
	};

	/* Program reader class */
	class codeReader {
	public:
		/* Default constructor,
		   just initialize everything with
		   zeros */
		codeReader();
		/* Explicit constructor, from file.
		   Initialize everything with zeros and
		   set the input file to the one given */
		explicit codeReader(::std::FILE *in);

		/* Set input file */
		void initializeInputFile(::std::FILE *in);
		/* Read input program head */
		void readProgramHeader();
		/* Read instruction by its id */
		instruction getInstruction(size_t id);
		/* Get program information */
		codeData getProgramData() const;
		/* Get file by instruction id */
		::std::string getInstructionFile(size_t id);
		/* Get file beginning in code */
		size_t getFileBeginning(::std::string fname);
		/* Get file ending in code */
		size_t getFileEnding(::std::string fname);
		/* Get line, where instruction placed in source on */
		size_t getInstructionLine(size_t id);
		/* Check the program for ability to be debugged */
		bool forDebugRun();
		/* Check for being ok */
		bool OK() const;

	private:
		/* Input file */
		::std::FILE *input;
		/* Input program data */
		codeData data;
	};

	::std::string getArgMnemonic(arg argInfo);
	::std::string getInstructionMnemonic(instruction inst);
}

#endif /* CODE_READER_HPP */
