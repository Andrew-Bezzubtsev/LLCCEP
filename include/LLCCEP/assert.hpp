#ifndef LLCCEP_ASSERT_HPP
#define LLCCEP_ASSERT_HPP

#include <LLCCEP/STDExtras.hpp>

#define LLCCEP_ASSERT(c) \
{ \
	bool expr = static_cast<bool>((c)); \
	if (!expr) { \
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG( \
			"LLCCEP assertion failed:\n" \
			"%s is invalid", \
			#c)); \
	} \
}

#endif /* LLCCEP_ASSERT_HPP */
