/**
 * @file basicBlock.hpp
 * 
 * $Copyright: (C) Andrew Bezzubtsev, 2016 $
 *
 * @par Basic block library.
 * This library provides you a class, made
 * for storing basic block data.
 * Basic block in LLCCEP bytecode becomes
 * just an address, where to jump
 * 
 * It is used in IR generation to make labels and,
 * if needed, to dump information about them.
 */
#ifndef LLCCEP_BASIC_BLOCK_HPP
#define LLCCEP_BASIC_BLOCK_HPP

#include <string>

namespace LLCCEP {
	/**
	 * @ingroup IR
	 *
	 * This class contains data about labels, made
	 * in program by user or a LLCCEP IR code
	 * generator. Every basic block may 
	 * be represented with its name.
	 * Also, it can be anonymous, if needed.
	 * But, there will be a special name synthesed for it.
	 *
	 * @code
	 * LLCCEP::basicBlock generateFunction(LLCCEP::IRGenerator &gen, node *f) {
	 *     LLCCEP::basicBlock functionLabel(f->children()[0].str, gen.pc()); 
	 *     generateConstantExpression(gen, f->children()[1]);
	 *
	 *     return functionLabel;
	 * }
	 * @endcode
	 *
	 * @see value, constantValue, registerValue, ramValue, IRGenerator
	 */
	class basicBlock {
		static size_t anonCounter = 0;
		size_t position;
		::std::string blockName;

	public:
		/**
		 * Default constructor.
		 * Syntheses an anonymous name for basic block,
		 * and sets its PC to the beginning of program.
		 *
		 * @code
		 * LLCCEP::basicBlock generateFunction(LLCCEP::IRGenerator &gen, node *f) {
		 *     LLCCEP::basicBlock functionLabel;
		 *     functionLabel.name(f->children()[0].str);
		 *     functionLabel.address(gen.pc());
		 *
		 *     generateConstantExpression(gen, f->children()[0]);
		 *     return functionLabel;
		 * }
		 * @endcode
		 * @see value, constantValue, registerValue, ramValue, IRGenerator, ~basicBlock()
		 */
		basicBlock();

		/**
		 * Constructor.
		 * Set name to a given one(defined by user application)
		 * and mark PC to a given one.
		 *
		 * @param name a name of basic block being created
		 * @param pos PC of basic block first instruction
		 *
		 * @code
		 * LLCCEP::basicBlock generateFunction(LLCCEP::IRGenerator &gen, node *f) {
		 *     LLCCEP::basicBlock functionLabel(f->children()[0].str, gen.pc());
		 *     generateConstantExpression(gen, f->children()[1]);
		 *
		 *     return functionLabel;
		 * }
		 * @endcode
		 *
		 * @see value, constantValue, registerValue, ramValue, IRGenerator, ~basicBlock()
		 */
		explicit basicBlock(::std::string name, size_t pos = 0);

		/**
		 * Constructor.
		 * Create anonymous basic block with set pc to a given one
		 *
		 * @param pos PC of basic block first instruction
		 *
		 * @code
		 * LLCCEP::basicBlock generateFunction(LLCCEP::IRGenerator &gen, node *f) {
		 *     LLCCEP::basicBlock functionLabel(gen.pc());
		 *     generateConstantExpression(gen, g->children()[1]);;
		 *
		 *     return functionLabel;
		 * }
		 * @endcode
		 *
		 * @see value, constantValue, registerValue, ramValue, IRGenerator, ~basicBlock()
		 */
		explicit basicBlock(size_t pos);
		
		/**
		 * Default destructor.
		 * Just deletes the object.
		 * No call needed in case of default creation
		 *
		 * @see basicBlock()
		 */
		~basicBlock();

		/**
		 * Get name of basic block
		 *
		 * @code
		 * LLCCEP::basicBlock bb;
		 * printf("Just synthesed %s basic block", bb.name().c_str());
		 * @endcode
		 *
		 * @return name of basic block as std::string object
		 *
		 * @see address()
		 */
		::std::string name() const;

		/**
		 * Set name of basic block
		 *
		 * @code
		 * LLCCEP::basicBlock bb;
		 * printf("Just synthesed %s renamed to xxxyyy", bb.name().c_str());
		 * bb.name("xxxyyy");
		 * @endcode
		 *
		 * @see address()
		 */
		void name(::std::string newName);

		/**
		 * Get PC of basic block
		 * 
		 * @code
		 * LLCCEP::basicBlock bb = getFunction();
		 *
		 * printf("%zu is function basic block", bb.address());
		 * @endcode
		 *
		 * @see name()
		 */
		size_t address() const;

		/**
		 * Set PC of basic block
		 *
		 * @code
		 * LLCCEP::basicBlock bb;
		 *
		 * bb.address(getFunctionPC());
		 * @endcode
		 *
		 * @see name()
		 */ 
		void address(size_t newAddress);
	};
}

#endif /* LLCCEP_BASIC_BLOCK_HPP */
