/**
 * @file value.hpp
 *
 * $Copyright: (C) Andrew Bezzubtsev, 2016 $
 *
 * @par This file contains classes for storing and handling data about
 * registers, memory cells and constant numeric values
 */
#ifndef LLCCEP_VALUE_HPP
#define LLCCEP_VALUE_HPP

#include <string>

namespace LLCCEP {
	/**
	 * @ingroup IR
	 *
	 * Value base class.
	 * This class is made to generalize code generation
	 * algorithm, using value
	 * It consists of empty constructor, destructor and
	 * a couple of pure virtual functions, declared
	 * in his children.
	 * This class is made just for generalization,
	 * it is the one usage of it.
	 *
	 * @code
	 * class generalizedValue: public value {
	 *     ...
	 * };
	 * @endcode
	 *
	 * @see registerValue, ramValue, constantValue, IRGenerator
	 */
	class value {
	public:
		/**
		 * Default constructor
		 */
		value();

		/**
		 * Default destructor
		 */
		virtual ~value();

		/**
		 * Pure virtual function
		 * for getting LLCCEP IR mnemonics for
		 * values, who can be registers, values, ram pointers...
		 *
		 * @code
		 * void printMov(LLCCEP::value *v0, LLCCEP::value *v1) {
		 *      ::std::cout << "mov " << v0->getMnemonic() << ", "
		 *                  << v1->getMnemonic << "\n";
		 * }
		 * @endcode
		 *
		 * @return value mnemonic native-to-human representation in std::string
		 */
		virtual ::std::string getMnemonic() const = 0;
		
		/**
		 * Pure virtual function for 
		 * debug dump of value to stderr
		 *
		 * @code
		 * for (const auto &i: registeredValues) {
		 *     i.dump();
		 * }
		 * @endcode
		 */
		virtual void dump() const = 0;
	};

	/**
	 * @ingroup IR
	 *
	 * Register value.
	 * The register value is defined by
	 * just one parameter - the ID of
	 * register where it is being
	 * stored.
	 * This class in LLCCEP is just an instance of value class.
	 * It is not used obviously, but via using value base class...
	 *
	 * @code
	 * void movToReg(LLCCEP::registerValue *dst, double d) {
	 *     ::std::cout << "mov " << dst->getMnemonic() << ", "
	 *                 << d << "\n";
	 * }
	 * @endcode
	 *
	 * @see value, ramValue, constantValue, IRGenerator
	 */
	class registerValue: public value {
		size_t regId;

	public:
		/**
		 * Default constructor.
		 * Initializes register's id with zero
		 */
		registerValue();

		/**
		 * Constructor.
		 * Sets id to a given one.
		 *
		 * @param id the id of register
		 *
		 * The id should be in range [0, 31], because
		 * there are only 32 registers in LLCCEP abstract machine
		 * 
		 * @code
		 * builder.insertMov(registerValue(0), constantValue(0));
		 * @endcode
		 *
		 * @see id()
		 */
		explicit registerValue(size_t id);

		/**
		 * Get ID of register used to store it
		 *
		 * @code
		 * printf("%zu register is used for Pi evalution...", reg.id());
		 * @endcode
		 *
		 * @return identifier of register used
		 *
		 * @see getMnemonic()
		 */
		size_t id() const;

		/**
		 * Substitute ID of register used as value storage
		 *
		 * @code
		 * printf("11 register is used, switching to 12...");
		 * reg.id(12);
		 * @endcode
		 *
		 * @see getMnemonic()
		 */
		void id(size_t newId);

		/**
		 * Get mnemonic for register value
		 * This function just returns '&' followed by register id
		 *
		 * @code
		 * void mov(LLCCEP::registerValue r, LLCCEP::registerValue s) {
		 *     ::std::cout << "mov " << r.getMnemonic() << ", " << s.getMnemonic();
		 * }
		 * @endcode
		 *
		 * @see id(), value
		 */
		::std::string getMnemonic() const override;
		void dump() const;

	private:
		void check() const;
	};

	/**
	 * @ingroup IR
	 *
	 * Ram value
	 * This class represents a value stored in a ram cell
	 * It has just a virtual address of cell
	 *
	 * @code
	 * void mov(LLCCEP::ramValue cell, double d) {
	 *     ::std::cout << "mov " << cell.getMnemonic() << ", " << d
	 *                 << ::std::endl;
	 * }
	 * @endcode
	 *
	 * @see value, registerValue, constantValue, IRGenerator
	 */
	class ramValue: public value {
		size_t vaddress;

	public:
		/**
		 * Default constructor, initializes address with
		 * zero
		 *
		 * @code
		 * LLCCEP::ramValue null;
		 * if (cell.vaddr() == null.vaddr()) {
		 *     throw 0xC;
		 * }
		 * @endcode
		 *
		 * @see vaddr(), value, constantValue, registerValue, IRGenerator
		 */
		ramValue();

		/**
		 * Constructor.
		 * Initializes address with a given value
		 * 
		 * @param addr address to be set
		 *
		 * @code
		 * LLCCEP::ramValue null(0);
		 * if (cell == null) {
		 *     throw 0xC;
		 * }
		 * @endcode
		 *
		 * @see vaddr(), value, constantValue, registerValue, IRGenerator
		 */
		explicit ramValue(size_t addr);

		/**
		 * Get virtual address of memory cell used
		 *
		 * @code
		 * LLCCEP::ramValue cell(rand() % 255);
		 * printf("Using %zu cell...", cell.vaddr());
		 * @endcode
		 *
		 * @return virtual address of memory cell used
		 *
		 * @see getMnemonic(), ramValue(), constantvalue, registerValue, IRGenerator
		 */
		size_t vaddr() const;
		
		/**
		 * Set virtual address of memory cell used
		 *
		 * @code
		 * LLCCEP::ramValue cell;
		 * cell.vaddr(rand() % 255);
		 * printf("Using %zu cell...", cell.vaddr());
		 * @endcode
		 *
		 * @see getMnemonic(), ramValue(), constantValue, registerValue, IRGenerator
		 */
		void vaddr(size_t newVaddr);

		/**
		 * Get mnemonic for memory cell.
		 * Just returns '$' followed by vaddr as an std::string
		 *
		 * @code
		 * void mov(LLCCEP::ramValue v, double d) {
		 *     ::std::cout << "mov " << v.getMnemonic() << ", " << d << ::std::endl;
		 * }
		 * @endcode
		 *
		 * @return native-to-human mnemonical representation of cell address as std::string
		 *
		 * @see vaddr(), constantValue, registerValue, IRGenerator
		 */
		::std::string getMnemonic() const override;
		void dump() const;
	};

	/**
	 * @ingroup IR
	 *
	 * Constant value
	 * This class represents a constant double-percision float-point value.
	 * Just its value...
	 *
	 * @code
	 * void generate(LLCCEP::value *val);
	 *
	 * void test() {
	 *     generate(LLCCEP::constantValue(55.0f));
	 * }
	 * @endcode
	 *
	 * @see value, registerValue, ramValue, IRGenerator
	 */
	class constantValue: public value {
		double val;

	public:
		/**
		 * Default constructor.
		 * Initializes constant value with zero
		 *
		 * @code
		 * LLCCEP::constantValue zero;
		 * for (auto &val: constantValues) {
		 *      val.value(zero.value());
		 * }
		 * @endcode
		 *
		 * @see value(), value, registerValue, ramValue, IRGenerator
		 */
		constantValue();

		/**
		 * Constructor.
		 * Initializes constant value with a
		 * given one.
		 *
		 * @param v constant value to be set
		 *
		 * @code
		 * LLCCEP::constantValue cv(55.0f);
		 * printf("Created constant value(%lg)\n", cv.value();
		 * @endcode
		 *
		 * @see value(), value, registerValue, ramValue, IRGenerator
		 */
		explicit constantValue(double v);

		/**
		 * Get constant value as double.
		 *
		 * @code
		 * void dump(LLCCEP::constantValue val) {
		 *     fprintf(stderr, "FP double-percision(value = %lg)", val.value());
		 * }
		 * @endcode
		 *
		 * @return stored constant value
		 *
		 * @see getMnemonic(), value, registerValue, ramValue, IRGenerator
		 */
		double value() const;

		/**
		 * Substitute constant value.
		 *
		 * @param v a new constant value to be set
		 *
		 * @code
		 * void zeroMemory() {
		 *     for (auto &i: cells) {
		 *         i.value(0.0f);
		 *     }
		 * }
		 * @endcode
		 *
		 * @see getMnemonic(), value, registerValue, ramValue, IRGenerator
		 */
		void value(double v);

		/**
		 * Get string native-to-human representation
		 * of constant value. Just cast value to string
		 *
		 * @code
		 * void dump(LLCCEP::value *val) {
		 *      assert(val);
		 *      fprintf(stderr, "%s", val->getMnemonic().c_str());
		 * @endcode
		 *
		 * @return native-to-human mnemonical representation of value as std::string
		 *
		 * @see value(), value, registerValue, ramValue, IRGenerator
		 */
		::std::string getMnemonic() const override;
		void dump() const;
	};
}

#endif /* LLCCEP_VALUE_HPP */
