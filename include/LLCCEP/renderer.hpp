#ifndef LLCCEP_RENDERER_HPP
#define LLCCEP_RENDERER_HPP

namespace LLCCEP {
	class renderer: public QWidget {
		Q_OBJECT

	public:
		renderer(QWidget *parent = NULL);
		virtual ~renderer() Q_DECL_OVERRIDE;

		void begin(int w, int h);

		void setAffineTransformData(double tX, double tY,
		                            double alpha,
					    double sX, double sY);
		void setPen(const QPen &pen);
		void setAntialiased(bool setAntialiased = trie);
		void lock();
		void end();

		QPainter &painter() const;
		QImage getImage() const;

		bool ok() const;

	protected:
		virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
		virtual void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;

	private:
		QTimer *update_timer;
		QPainter *image_painter;
		QPixmap *framebuf;
		bool antialiased;
		bool started;
		bool locked;
	};
}

#endif /* LLCCEP_RENDERER_HPP */
