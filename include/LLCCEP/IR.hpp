#ifndef LLCCEP_IR_HPP
#define LLCCEP_IR_HPP

#include <string>
#include <vector>
#include <map>
#include <cstdio>

#include <LLCCEP/variable.hpp>
#include <LLCCEP/basicBlock.hpp>
#include <LLCCEP/value.hpp>

namespace LLCCEP {
	/**
	 * @ingroup IR
	 *
	 * A main class for construction of IR code.
	 * Uses such subclasses as value and basicBlock to
	 * reach the most flexible code generation.
	 *
	 * Generated code comes not very good readable
	 * to human, because the IR generator automatically
	 * inlines labels and variables addresses.
	 */
	class IRGenerator {
		size_t freeptr;
		::std::vector<size_t> released;	
		size_t pc;
		::std::FILE *output;

	public:
		IRGenerator();
		~IRGenerator();

		variable createVariable();
		basicBlock createLabelAtPC();
		void releaseVariable(variable toRelease);

		size_t PC() const;

		void insertMov(value out, value in);
		void insertMav(value out, value in);
		void insertMva(value out, value in);

		void insertPush(value val);
		void insertPop();
		void insertTop(value out);
		
		void insertAdd(value out, value i0, value i1);
		void insertSub(value out, value i0, value i1);
		void insertMul(value out, value i0, value i1);
		void insertDiv(value out, value i0, value i1);
		
		void insertAnd(value out, value i0, value i1);
		void insertOr(value out, value i0, value i1);
		void insertXor(value out, value i0, value i1);
		void insertOff(value out, value i0, value i1);
		
		void insertNop();
		void insertSwi(value id);
		void insertCmp(value i0, value i1);
		
		void insertInc(value i0);
		void insertDec(value i0);
		
		void insertSqrt(value out, value i0);
		void insertSin(value out, value i0);
		void insertCos(value out, value i0);
		void insertPtan(value out, value i0);
		void insertPatan(value out, value i0);
		void insertLdc(value out, value i0);

		void insertCall(basicBlock bb);
		void insertJmp(basicBlock bb);
		void insertRet();
		
		void insertStregs();
		void insertLdregs();

		void outputFile(::std::FILE *out = stdout);

	private:
		bool ok() const;
	};
}

#endif /* LLCCEP_IR_HPP */
