#ifndef LLCCEP_VARIABLE_HPP
#define LLCCEP_VARIABLE_HPP

#
namespace LLCCEP {
	class variable {
		size_t ptr;

	public:
		variable();
		variable(size_t newPtr);
		~variable();

		void pointer(size_t newPtr);
		size_t pointer() const;
	};
}
