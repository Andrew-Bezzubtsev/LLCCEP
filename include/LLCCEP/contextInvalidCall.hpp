#ifndef LLCCEP_CONTEXT_INVALID_CALL_HPP
#define LLCCEP_CONTEXT_INVALID_CALL_HPP

#include <LLCCEP/compileTime.hpp>
#include <LLCCEP/STDExtras.hpp>

#define LLCCEP_CONTEXT_INVALID_CALL \
{ \
	throw RUNTIME_EXCEPTION(CONSTRUCT_MSG( \
		"Context-invalid call of %s(see %s:%d", \
		LLCCEP_FUNCTION, LLCCEP_FILE, LLCCEP_LINE)); \
}

#endif /* LLCCEP_CONTEXT_INVALID_CALL_HPP */
