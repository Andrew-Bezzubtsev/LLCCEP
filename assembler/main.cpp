#include <iostream>
#include <cerrno>
#include <cstring>
#include <cstdlib>
#include <vector>

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/command-line.hpp>

#include "compiler/compiler.hpp"

int main(int argn, char **argv)
{	
	try {	
		LLCCEP_tools::commandLineParametersParser clpp;
		clpp.addFlag(LLCCEP_tools::commandLineFlag{{"-h", "--help"},
		                                           "help", false});
		clpp.addFlag(LLCCEP_tools::commandLineFlag{{"-o", "--output"},
		                                           "output", true});
		clpp.addFlag(LLCCEP_tools::commandLineFlag{{"-d", "--debug"},
		                                           "debug", false});
		clpp.setHelpText("LLCCEP assembler help.\n"
				 "-h/--help -- show help(this text)\n"
				 "-o/--output -- set output file(a path is after)[a.exec by default]\n"
				 "-d/--debug -- require assembler to produce debug symbols\n");
		clpp.setMaxFreeParams(-1);
		clpp.parse(argn, argv);

		if (clpp.getParam("help") == "1" ||
		    !clpp.getFreeParams().size()) {
			if (!clpp.getFreeParams().size())
				::std::cerr << "No input!\n";

			clpp.showHelp();
		}

		LLCCEP_ASM::compiler compiler;
		::std::string output = clpp.getParam("output").length()?
		                       clpp.getParam("output"):
				       "a.exec";
		auto input = clpp.getFreeParams();
		compiler.compile(input, output, clpp.getParam("debug") == "1");
	} DEFAULT_HANDLING

	return 0;
}
