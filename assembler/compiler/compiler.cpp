#include <vector>
#include <string>
#include <fstream>
#include <cstdio>

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/STLExtras.hpp>
#include <LLCCEP/convert.hpp>

#include "./../lexer/lexer.hpp"
#include "./../linker/linker.hpp"
#include "./../codegen/codegen.hpp"
#include "./../preprocessor/preprocessor.hpp"

#include "compiler.hpp"

LLCCEP_ASM::compiler::compiler()
{ }

LLCCEP_ASM::compiler::~compiler()
{ }

void LLCCEP_ASM::compiler::compile(::std::vector<::std::string> in, ::std::string out,
                                   bool debugInfo)
{
	/* Global preprocessing data */
	LLCCEP_ASM::preprocessor prep;
	/* Global linker data */
	LLCCEP_ASM::linker link;
	/* Global code generator data */
	LLCCEP_ASM::codeGenerator codegen;
	/* Program counter */
	size_t pc = 0;

	/* Beginnings of files */
	::std::vector<size_t> beginnings;

	/* Output file */
	::std::FILE *output;
	OPEN_FILE(output, out.c_str(), "w");

	/* Generate code for lexems */
	auto generateCode = [&codegen, &output, debugInfo](::std::vector<LLCCEP_ASM::lexem> lexems) {
		/* If there is code */
		if (lexems.size()) {
			/* If needed debug info */
			if (debugInfo) {
				/* Write line of instruction in code */
				dump_bytes(output, to_bytes(lexems[0].pos.line));
			}

			/* Generate code */
			codegen.dumpOperationBitset(output, codegen.prepareOperation(lexems));
		}
	};

	/* Do two passes */
	for (unsigned pass = 0; pass < 2; pass++) { 
		/* If second reached */
		if (pass) {
			/* Does code contain debug info or not */
			::std::fputc(static_cast<uint8_t>(debugInfo), output);
			/* Word size for the machine */
			::std::fputc(static_cast<uint8_t>(sizeof(size_t)), output);

			/* Offset to main instruction */
			dump_bytes(output, to_bytes(link.getMainAddress()));
	
			/* If needed to produce debug info */
			if (debugInfo) {
				/* Amount of files used in build */
				dump_bytes(output, to_bytes(in.size()));
				/* For all files used */
				for (size_t j = 0; j < in.size(); j++) {
					/* Write filename */
					::std::fputs(in[j].c_str(), output);
					::std::fputc(0, output);

					/* Write place the file begins in */
					dump_bytes(output, to_bytes(beginnings[j]));
				}
			}
		}

		/* For all given paths */
		for (const auto &i: in) {
			/* Open the file with the path */
			FILE *in = NULL;
			OPEN_FILE(in, i.c_str(), "r");

			/* Create & attach lexer to file */
			LLCCEP_ASM::lexer lex;
			lex.setProcessingPath(i);
			lex.setProcessingFile(in);

			/* Save the place file begins */
			beginnings.push_back(pc);

			/* For entire file */
			while (!feof(in)) {
			       /* Get next line */	
				::std::vector<LLCCEP_ASM::lexem> lexems; 
				lex.getNextLine(lexems); 

				/* If it's first pass */
				if (!pass) {		
					/* If preprocessor's code */
					if (prep.preprocessorCode(lexems)) {
						/* Skip it */
						continue;
					}

					/* Save labels addresses */
					link.buildLabelsAssociativeTable(lexems, pc);

					/* If there was no declaration, but code was */
					if (!link.hasDeclaration(lexems) && lexems.size()) {
						/* Increment program counter */
						pc++;
					}
				} else {
					::std::vector<LLCCEP_ASM::lexem> newLexems;

					/* If code doesn't belong to preprocessor */
				        if (!prep.preprocessorStuff(lexems)) {
						/* Preprocess it */
						prep.preprocessCode(lexems, newLexems);
					/* If it belongs to preprocessor */
					} else {
						/* Skip it */
						continue;
					}

					/* Declare/delete variables if needed */
					link.modifyVariablesTable(newLexems);
					/* If there was no variable declaration/releasement and
					   there is some code */
					if (!link.hasDeclaration(newLexems) && newLexems.size()) {
						lexems.clear();
					
						/* For all lexems */
						for (const auto &i: newLexems) {
							/* If EOL met */
							if (i.type == LLCCEP_ASM::LEX_T_NEWLINE) {
								/* Generate code for instruction */
								link.substituteWithAddresses(lexems);
								generateCode(lexems);
								/* Clear lexems */
								lexems.clear();
							/* Until EOL */
							} else {
								/* Emit lexem to end */
								lexems.push_back(i);
							}
						}

						/* Generate code for left instruction */
						link.substituteWithAddresses(lexems);
						generateCode(lexems);	
					}
				}
			}

			::std::fclose(in);
		}
	}

	::std::fclose(output);
}
