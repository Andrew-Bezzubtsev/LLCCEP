#include <vector>
#include <string>
#include <map>

#include <cstddef>
#include <cstdarg>

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/STLExtras.hpp>
#include <LLCCEP/os-specific.hpp>
#include <LLCCEP/convert.hpp>

#include "../lexer/lexer.hpp"
#include "linker.hpp"

LLCCEP_ASM::linker::linker():
	stored(),
	releasedMemory()
{ }

LLCCEP_ASM::linker::~linker()
{ }

bool LLCCEP_ASM::linker::hasDeclaration(::std::vector<lexem> lex) const
{
	return hasVariableModification(lex) || hasLabelDeclaration(lex);
}

void LLCCEP_ASM::linker::modifyVariablesTable(::std::vector<lexem> lex)
{
	/* Check for begin declaration */	
	auto isVariableDeclaration = [lex] {
		return lex.size() && lex[0].type == LLCCEP_ASM::LEX_T_VAR;
	};

	/* Check for being deletion */
	auto isVariableDeletion = [lex] {
		return lex.size() && lex[0].type == LLCCEP_ASM::LEX_T_RELEASE;
	};

	/* Get last variable declared */
	auto lastVariable = [this] {
		for (auto i = stored.begin(); i != stored.end(); i++)
			if (!i->second.label)
				return i;

		return stored.end();
	};

	/* Create variable */
	auto createVariable = [this, lastVariable, isVariableDeclaration, lex] {
		if (!isVariableDeclaration())
			return;

		auto pos = stored.find(lex[1].val);
		if (pos != stored.end()) {
			linkerIssue(lex[1], "%s was already declared in '%s' file "
			                    "in line " size_t_pf,
					    lex[1].val.c_str(),
					    pos->second.lexemData.pos.file.c_str(),
					    pos->second.lexemData.pos.line);
		}

		saveData newVariable{lex[1], 0, false};
		if (releasedMemory.size()) {
			newVariable.pos = *(releasedMemory.end() - 1);
			releasedMemory.pop_back();
		} else if (lastVariable() != stored.end()) {
			newVariable.pos = lastVariable()->second.pos + 1;
		} else {
			newVariable.pos = 32;
		}
		
		stored[lex[1].val] = newVariable;
	};

	/* Delete variable */
	auto deleteVariable = [this, isVariableDeletion, lex] {
		if (!isVariableDeletion())
			return;

		auto pos = stored.find(lex[1].val);
		if (pos == stored.end()) {
			linkerIssue(lex[1], "'%s' was not declared yet",
			            lex[1].val.c_str());
		}

		releasedMemory.push_back(pos->second.pos);
		stored.erase(pos);	
	};

	if (isVariableDeclaration())
		createVariable();
	else if (isVariableDeletion())
		deleteVariable();
}

void LLCCEP_ASM::linker::buildLabelsAssociativeTable(::std::vector<lexem> lex,
                                                     size_t iteration)
{	
	if (!hasLabelDeclaration(lex))
		return;

	auto pos = stored.find(lex[0].val);
	if (pos != stored.end()) {
		linkerIssue(lex[0], "'%s' was already declared at file '%s' on "
		                    "line " size_t_pf,
				    lex[0].val.c_str(),
				    pos->second.lexemData.pos.file.c_str(),
				    pos->second.lexemData.pos.line);
	}

	saveData newLabel = saveData{lex[0], iteration, true};
	stored[lex[0].val] = newLabel;
}

void LLCCEP_ASM::linker::substituteWithAddresses(::std::vector<lexem> &lexems)
{
	auto substituteName = [this](LLCCEP_ASM::lexem &lexemData) {
		if (lexemData.type != LLCCEP_ASM::LEX_T_NAME) {
			throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
				"Attempt to substitute noname lexem"));
		}

		auto pos = stored.find(lexemData.val);
		if (pos == stored.end()) {
			linkerIssue(lexemData, "'%s' was not declared yet",
			            lexemData.val.c_str());
		}

		if (pos->second.label)
			lexemData.type = LLCCEP_ASM::LEX_T_VAL;
		else
			lexemData.type = LLCCEP_ASM::LEX_T_MEM;

		lexemData.val = to_string(pos->second.pos);
	};

	bool instruction = true;
	for (auto &i: lexems) {
		if (instruction) {
			instruction = false;
			continue;
		}

		if (i.type == LLCCEP_ASM::LEX_T_NAME)
			substituteName(i);
	}
}

size_t LLCCEP_ASM::linker::getMainAddress() const
{
	auto pos = stored.find("_main");
	if (pos == stored.end())
		linkerIssue(LLCCEP_ASM::lexem{}, "'_main' was not declared yet");

	if (!pos->second.label)
		linkerIssue(pos->second.lexemData, "'_main' is declared as not label");
	
	return pos->second.pos;
}

void LLCCEP_ASM::linker::linkerIssue(LLCCEP_ASM::lexem issuedLabel, const char *fmt, ...) const
{
	va_list list;
	va_start(list, fmt);

	char res[4096] = "";
	vsprintf(res, fmt, list);

	va_end(list);

	::std::string absPath = filesystem::getAbsolutePath(issuedLabel.pos.file);
	throw RUNTIME_EXCEPTION(CONSTRUCT_MSG("%s:" size_t_pf ":\n%s", absPath.c_str(),
				issuedLabel.pos.line, res));
}

bool LLCCEP_ASM::linker::hasLabelDeclaration(::std::vector<LLCCEP_ASM::lexem> lex) const
{
	if (lex.size() >= 2 && lex[0].type == LLCCEP_ASM::LEX_T_NAME &&
	    lex[1].type == LLCCEP_ASM::LEX_T_COLON) {
		if (lex.size() > 2) {
			linkerIssue(lex[0],
				    "Junk lexems after '%s' label declaration",
				    lex[0].val.c_str());
		}

		return true;
	}

	return false;
}

bool LLCCEP_ASM::linker::hasVariableModification(::std::vector<LLCCEP_ASM::lexem> lex) const
{
	if (lex.size() && (lex[0].type == LLCCEP_ASM::LEX_T_VAR ||
	                   lex[0].type == LLCCEP_ASM::LEX_T_RELEASE)) {
		if (lex.size() < 2 || lex[1].type != LLCCEP_ASM::LEX_T_NAME) {
			linkerIssue(lex[0], 
				    "Excepted variable name after '%s'",
				    (lex[0].type == LLCCEP_ASM::LEX_T_VAR)?
				    ("var"):
				    ("release"));
		} else if (lex.size() > 2) {
			linkerIssue(lex[0],
				    "Junk lexems after '%s' variable %s",
				    lex[1].val.c_str(),
				    (lex[0].type == LLCCEP_ASM::LEX_T_VAR)?
				    ("declaration"):
				    ("deletion"));
		}

		return true;
	}

	return false;
}

