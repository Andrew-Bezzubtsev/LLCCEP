# Disassembler of LLCCEP

## About
This program translates LLCCEP VM bytecode into LLCCEP assembly
language source, which can be edited, if the source is lost.

## Building the disassembler
Build dependencies: C++ compiler, supporting GNU++14 language 
standard, CMake utility.

1) Generate build reciepe for your IDE/just Makefile.

    cmake . -DBUILD_MODE=xxxxxxx

xxxxxxx is target type. It may be:
* Debug, to build disassembler for debugging
* Release, to build disassembler for routine usage

2) Build disassembler using your IDE or Make utility.

The build was tested on the Clang compiler. Building on GCC also
OK, but is not frequently tested, as I prefer using Clang.

## Command-line parameters
The assembler if intended for command-line usage. It has got
some specific options, to make usage more flexible.<br>
Command-line parameters(options):
* --help/-h -- show help text
* --output/-o -- path after is output file's one(a.asm by default)
* There should be single free parameter, which will be input program.


