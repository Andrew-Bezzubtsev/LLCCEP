#include <llvm/ADT/APFloat.h>
#include <llvm/ADT/APInt.h>
#include <llvm/ADT/STLExtras.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Verifier.h>
#include <llvm/Linker/Linker.h>
#include <llvm/Support/raw_ostream.h>

#include <llvm/ExecutionEngine/Orc/CompileUtils.h>

#include <LLCCEP/codeReader.hpp>
#include <LLCCEP/convert.hpp>

#include "../llvmModules/host.hpp"

#include "../moduleReader/moduleReader.hpp"
#include "backend.hpp"

LLCCEP::nativeBackend::nativeBackend(::std::string moduleName)
	: context(),
	  builder(context),
	  registersPtr(NULL),
	  heapPtr(NULL),
	  compareFlag(NULL),
	  mainFunction(NULL),
	  instructionsBB(),
	  currentIP(0),
	  currentModule(),
	  cr(NULL)
{
	currentModule = llvm::make_unique<llvm::Module>(moduleName.c_str(), context);
}

LLCCEP::nativeBackend::~nativeBackend()
{
}

void LLCCEP::nativeBackend::generateInitial()
{
	registersPtr = new llvm::GlobalVariable(*currentModule, llvm::ArrayType::get(llvm::Type::getDoubleTy(context), 32),
						false, llvm::GlobalVariable::ExternalLinkage, 0, "__registersPtr");
	registersPtr->setAlignment(16);
	registersPtr->setInitializer(llvm::ConstantAggregateZero::get(llvm::ArrayType::get(llvm::Type::getDoubleTy(context), 32)));

	compareFlag = new llvm::GlobalVariable(*currentModule, llvm::Type::getInt64Ty(context), false, llvm::GlobalVariable::ExternalLinkage,
					       0, "__compareFlag");
	compareFlag->setAlignment(8);
	compareFlag->setInitializer(llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), 0x8));

	//llvm::Linker::linkModules(*currentModule, LLCCEP::loadLLVMModule(context, "host", LLCCEP::hostModule));

	heapPtr = currentModule->getGlobalVariable("__heapPtr");

	mainFunction = llvm::Function::Create(llvm::FunctionType::get(llvm::Type::getVoidTy(context), false),
					      llvm::Function::ExternalLinkage, "main", currentModule.get());

	auto bb = llvm::BasicBlock::Create(context, "", mainFunction);
	builder.SetInsertPoint(bb);
}

void LLCCEP::nativeBackend::generateDeinitial()
{
	builder.CreateRetVoid();
}

void LLCCEP::nativeBackend::setCodeReader(LLCCEP::codeReader *newCr)
{
	cr = newCr;
}

void LLCCEP::nativeBackend::generateCode()
{
	cr->readProgramHeader();

	generateInitial();
	builder.CreateCall(getModuleFunction("initStack"));
	builder.CreateCall(getModuleFunction("initCall"));
	builder.CreateCall(getModuleFunction("initRegistersStorage"));

	for (size_t i = 0; i < cr->getProgramData().size; i++)
		instructionsBB.push_back(llvm::BasicBlock::Create(context, ::std::string("_instruction") + to_string(i), mainFunction));
	instructionsBB.push_back(llvm::BasicBlock::Create(context, ::std::string("__end"), mainFunction));
	builder.CreateBr(getInstruction(cr->getProgramData().main_id));

	for (size_t i = 0; i < cr->getProgramData().size; i++) {
		builder.SetInsertPoint(getInstruction(i));
		generateInstructionCode(cr->getInstruction(i));

		currentIP++;
	}

	builder.SetInsertPoint(*(instructionsBB.end() - 1));

	generateDeinitial();
}

void LLCCEP::nativeBackend::generateInstructionCode(LLCCEP::instruction info)
{
	void (LLCCEP::nativeBackend::*functions[])(LLCCEP::instruction) = {
	&LLCCEP::nativeBackend::generateMov,
	&LLCCEP::nativeBackend::generateMav,
	&LLCCEP::nativeBackend::generateMva,
	&LLCCEP::nativeBackend::generatePush,
	&LLCCEP::nativeBackend::generatePop,
	&LLCCEP::nativeBackend::generateTop,
	&LLCCEP::nativeBackend::generateAdd,
	&LLCCEP::nativeBackend::generateSub,
	&LLCCEP::nativeBackend::generateMul,
	&LLCCEP::nativeBackend::generateDiv,
	&LLCCEP::nativeBackend::generateAnd,
	&LLCCEP::nativeBackend::generateOr,
	&LLCCEP::nativeBackend::generateXor,
	&LLCCEP::nativeBackend::generateOff,
	&LLCCEP::nativeBackend::generateNop,
	&LLCCEP::nativeBackend::generateSwi,
	&LLCCEP::nativeBackend::generateCmp,
	&LLCCEP::nativeBackend::generateInc,
	&LLCCEP::nativeBackend::generateDec,
	&LLCCEP::nativeBackend::generateSqrt,
	&LLCCEP::nativeBackend::generateSin,
	&LLCCEP::nativeBackend::generateCos,
	&LLCCEP::nativeBackend::generatePtan,
	&LLCCEP::nativeBackend::generatePatan,
	&LLCCEP::nativeBackend::generateLdc,
	&LLCCEP::nativeBackend::generateCall,
	&LLCCEP::nativeBackend::generateJmp,
	&LLCCEP::nativeBackend::generateRet,
	&LLCCEP::nativeBackend::generateStregs,
	&LLCCEP::nativeBackend::generateLdregs};

	if (info.opcode >= LLCCEP_ASM::INST_NUM) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
		"Opcode overbound for codegenerator!"));
	}

	(this->*functions[info.opcode])(info);
}

void LLCCEP::nativeBackend::dump() const
{
	currentModule->print(llvm::errs(), NULL, false, true);
}

void LLCCEP::nativeBackend::write(llvm::raw_ostream &out) const
{
	currentModule->print(out, NULL);
}

llvm::Function *LLCCEP::nativeBackend::getModuleFunction(::std::string name)
{
	return currentModule->getFunction(name);
}

llvm::Value *LLCCEP::nativeBackend::getInt(long long val)
{
	return llvm::ConstantInt::get(llvm::Type::getInt64Ty(context), val);
}

llvm::Value *LLCCEP::nativeBackend::getDouble(double val)
{
	return llvm::ConstantFP::get(llvm::Type::getDoubleTy(context), val);
}

llvm::Value *LLCCEP::nativeBackend::get(LLCCEP::arg data)
{
	switch (data.type) {
	case LLCCEP_ASM::LEX_T_COND:
	case LLCCEP_ASM::LEX_T_VAL: {
		return llvm::ConstantFP::get(llvm::Type::getDoubleTy(context), data.val);
	}

	case LLCCEP_ASM::LEX_T_MEM: {
		auto cellPtr = getPtr(data);
		return builder.CreateLoad(llvm::Type::getDoubleTy(context), cellPtr);
	}

	case LLCCEP_ASM::LEX_T_REG: {
		auto regPtr = getPtr(data);
		return builder.CreateLoad(llvm::Type::getDoubleTy(context), regPtr);
	}

	default: {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
		"Invalid binary: failed to synthese get"));
	}
	}

	return NULL;
}

llvm::Value *LLCCEP::nativeBackend::getPtr(LLCCEP::arg data)
{
	switch (data.type) {
	case LLCCEP_ASM::LEX_T_REG: {
		LLCCEP::nativeBackend::functionArguments idx;
		idx.push_back(getInt(0));
		idx.push_back(getInt(static_cast<size_t>(data.val)));

		auto getter = llvm::GetElementPtrInst::CreateInBounds(registersPtr, idx);
		builder.Insert(getter);

		return getter;
	}

	case LLCCEP_ASM::LEX_T_MEM: {
		LLCCEP::nativeBackend::functionArguments args;
		args.push_back(getInt(static_cast<size_t>(data.val)));
		builder.CreateCall(getModuleFunction("reallocateHeap"), args);

		args.insert(args.begin(), getInt(0));

		auto getter = llvm::GetElementPtrInst::CreateInBounds(heapPtr, args);
		builder.Insert(getter);
		return getter;
	}

	default: {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
		"Invalid binary: failed to synthese getPtr"));
	}
	}

	return NULL;
}

llvm::BasicBlock *LLCCEP::nativeBackend::getInstruction(size_t id)
{
	if (id >= instructionsBB.size())
		return *(instructionsBB.end() - 1);

	return instructionsBB[id];
}

void LLCCEP::nativeBackend::generateMov(LLCCEP::instruction info)
{
	auto copying = get(info.args[1]);
	auto where = getPtr(info.args[0]);

	builder.CreateStore(copying, where);
}

void LLCCEP::nativeBackend::generateMav(LLCCEP::instruction info)
{
	auto copying = get(info.args[1]);
	auto whereAddr = get(info.args[0]);

	LLCCEP::nativeBackend::functionArguments args;
	args.push_back(builder.CreateFPToUI(whereAddr, llvm::Type::getInt64Ty(context)));
	builder.CreateCall(getModuleFunction("reallocateHeap"), args);

	args.insert(args.begin(), getInt(0));
	auto ptr = llvm::GetElementPtrInst::CreateInBounds(heapPtr, args);
	builder.Insert(ptr);
	builder.CreateStore(copying, ptr);
}

void LLCCEP::nativeBackend::generateMva(LLCCEP::instruction info)
{
	auto copyPtr = get(info.args[1]);
	auto where = getPtr(info.args[0]);

	LLCCEP::nativeBackend::functionArguments args;
	args.push_back(builder.CreateFPToUI(copyPtr, llvm::Type::getInt64Ty(context)));
	builder.CreateCall(getModuleFunction("reallocateHeap"), args);

	args.insert(args.begin(), getInt(0));
	auto ptr = llvm::GetElementPtrInst::CreateInBounds(heapPtr, args);
	builder.Insert(ptr);
	builder.CreateStore(builder.CreateLoad(ptr), where);
}

void LLCCEP::nativeBackend::generatePush(LLCCEP::instruction info)
{
	LLCCEP::nativeBackend::functionArguments args;
	args.push_back(get(info.args[0]));

	builder.CreateCall(getModuleFunction("stackPush"), args);
}

void LLCCEP::nativeBackend::generatePop(LLCCEP::instruction info)
{
	builder.CreateCall(getModuleFunction("stackPop"));
}

void LLCCEP::nativeBackend::generateTop(LLCCEP::instruction info)
{
	auto ptr = getPtr(info.args[0]);
	auto top = builder.CreateCall(getModuleFunction("stackTop"));

	builder.CreateStore(top, ptr);
}

#define SYNTHESE_ARITHMETICS(op)                                           \
	void LLCCEP::nativeBackend::generate##op(LLCCEP::instruction info) \
	{                                                                  \
		llvm::Value *where = getPtr(info.args[0]);                 \
		llvm::Value *o[] = {get(info.args[1]), get(info.args[2])}; \
                                                                           \
		auto res = builder.CreateF##op(o[0], o[1]);                \
		builder.CreateStore(res, where);                           \
	}

SYNTHESE_ARITHMETICS(Add);
SYNTHESE_ARITHMETICS(Sub);
SYNTHESE_ARITHMETICS(Mul);
SYNTHESE_ARITHMETICS(Div);

#undef SYNTHESE_ARITHMETICS

#define SYNTHESE_LOGIC(op)                                                                              \
	void LLCCEP::nativeBackend::generate##op(LLCCEP::instruction info)                              \
	{                                                                                               \
		llvm::Value *where = getPtr(info.args[0]);                                              \
		llvm::Value *o[] = {get(info.args[1]),                                                  \
				    get(info.args[2])};                                                 \
                                                                                                        \
		for (unsigned i = 0; i < 2; i++) {                                                      \
			auto ui = builder.CreateFPToUI(o[i], llvm::Type::getInt64Ty(context));          \
			builder.CreateLoad(o[i], ui);                                                   \
		}                                                                                       \
                                                                                                        \
		auto res = builder.Create##op(o[0], o[1]);                                              \
		builder.CreateLoad(where, builder.CreateUIToFP(res, llvm::Type::getDoubleTy(context))); \
	}

SYNTHESE_LOGIC(And);
SYNTHESE_LOGIC(Or);
SYNTHESE_LOGIC(Xor);

#undef SYNTHESE_LOGIC

void LLCCEP::nativeBackend::generateOff(LLCCEP::instruction info)
{
	llvm::Value *o[] = {get(info.args[1]),
			    get(info.args[2])};
	llvm::Value *res = getPtr(info.args[0]);

	for (unsigned i = 0; i < 2; i++) {
		builder.CreateFPToUI(o[i], llvm::Type::getInt64Ty(context), "v0");
		builder.CreateLoad(o[i], "v0");
	}

	builder.CreateICmpSGE(o[1], getInt(0), "v0");
	builder.CreateLoad(res, "v0");
	builder.CreateShl(o[0], o[1], "v0");
	builder.CreateLoad(o[0], "v0");
	builder.CreateLShr(o[0], o[1], "v0");
	builder.CreateLoad(o[1], "v0");
	builder.CreateSelect(res, o[0], o[1], "v0");
	builder.CreateLoad(res, "v0");
}

void LLCCEP::nativeBackend::generateNop(LLCCEP::instruction info)
{
	/* To reach just greater speed */
}

void LLCCEP::nativeBackend::generateSwi(LLCCEP::instruction info)
{
	LLCCEP::nativeBackend::functionArguments fa;
	fa.push_back(builder.CreateFPToUI(get(info.args[0]), llvm::Type::getInt64Ty(context)));

	builder.CreateCall(getModuleFunction("interrupt"), fa);
}

void LLCCEP::nativeBackend::generateCmp(LLCCEP::instruction info)
{
}

#define SYNTHESE_INC_DEC(op, act)                                          \
	void LLCCEP::nativeBackend::generate##op(LLCCEP::instruction info) \
	{                                                                  \
		llvm::Value *res = getPtr(info.args[0]);                   \
		llvm::Value *o = get(info.args[0]);                        \
                                                                           \
		auto actRes = builder.Create##act(o, getDouble(1.0f));     \
		builder.CreateStore(actRes, res);                          \
	}

SYNTHESE_INC_DEC(Inc, Add);
SYNTHESE_INC_DEC(Dec, Sub);

#undef SYNTHESE_INC_DEC

#define SYNTHESE_MATH(op, name, calleeName)                                \
	void LLCCEP::nativeBackend::generate##op(LLCCEP::instruction info) \
	{                                                                  \
		llvm::Value *res = getPtr(info.args[0]);                   \
		llvm::Value *o = get(info.args[1]);                        \
                                                                           \
		auto callee = getModuleFunction(calleeName);               \
		LLCCEP::nativeBackend::functionArguments args;             \
		args.push_back(o);                                         \
                                                                           \
		builder.CreateLoad(builder.CreateCall(callee, args), res); \
	}

SYNTHESE_MATH(Sqrt, SQRT, "sqrt");
SYNTHESE_MATH(Sin, SIN, "sin");
SYNTHESE_MATH(Cos, COS, "cos");
SYNTHESE_MATH(Ptan, PTAN, "tan");
SYNTHESE_MATH(Patan, PATAN, "atan");
SYNTHESE_MATH(Ldc, LDC, "loadConstant");

#undef SYNTHESE_MATH

void LLCCEP::nativeBackend::generateCall(LLCCEP::instruction info)
{
	llvm::Value *condition = builder.CreateFPToUI(get(info.args[0]), llvm::Type::getInt64Ty(context));
	auto cond = builder.CreateTrunc(builder.CreateAnd(condition, builder.CreateLoad(compareFlag)), llvm::Type::getInt1Ty(context));

	auto callWrap = llvm::BasicBlock::Create(context, "_callWrap", mainFunction);

	builder.CreateCondBr(cond, callWrap, getInstruction(currentIP + 1));
	builder.SetInsertPoint(callWrap);

	LLCCEP::nativeBackend::functionArguments fa;
	fa.push_back(builder.CreatePtrToInt(llvm::BlockAddress::get(getInstruction(currentIP + 1)), llvm::Type::getInt64Ty(context)));

	builder.CreateCall(getModuleFunction("addCall"), fa);
	builder.CreateBr(getInstruction(static_cast<size_t>(info.args[1].val)));
}

void LLCCEP::nativeBackend::generateJmp(LLCCEP::instruction info)
{
	llvm::Value *condition = builder.CreateFPToUI(get(info.args[0]), llvm::Type::getInt64Ty(context));
	auto cond = builder.CreateAnd(condition, builder.CreateLoad(compareFlag));

	builder.CreateCondBr(builder.CreateTrunc(cond, llvm::Type::getInt1Ty(context)), getInstruction(static_cast<size_t>(info.args[1].val)),
			     getInstruction(currentIP + 1));
}

void LLCCEP::nativeBackend::generateRet(LLCCEP::instruction info)
{
	auto ret2 = builder.CreateCall(getModuleFunction("lastCall"));
	builder.CreateCall(getModuleFunction("deleteCall"));
	builder.CreateIndirectBr(builder.CreateIntToPtr(ret2, llvm::Type::getInt8PtrTy(context)));
}

void LLCCEP::nativeBackend::generateStregs(LLCCEP::instruction info)
{
	builder.CreateCall(getModuleFunction("storeRegisters"));
}

void LLCCEP::nativeBackend::generateLdregs(LLCCEP::instruction info)
{
	builder.CreateCall(getModuleFunction("loadRegisters"));
}
