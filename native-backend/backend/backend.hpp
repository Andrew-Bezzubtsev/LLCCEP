#ifndef LLCCEP_NATIVE_BACKEND_HPP
#define LLCCEP_NATIVE_BACKEND_HPP

#include <llvm/ADT/APFloat.h>
#include <llvm/ADT/STLExtras.h>
#include <llvm/IR/BasicBlock.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/DerivedTypes.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/Verifier.h>
#include <llvm/Support/raw_ostream.h>

/*#include <llvm/ExecutionEngine/Orc/ObjectLinkingLayer.h>*/
#include <llvm/ExecutionEngine/Orc/IRCompileLayer.h>

#include <LLCCEP/codeReader.hpp>

#include <map>
#include <memory>
#include <string>

namespace LLCCEP {
	class nativeBackend {
	protected:
		llvm::LLVMContext context;
		llvm::IRBuilder<> builder;

		llvm::GlobalVariable *registersPtr;
		llvm::GlobalVariable *heapPtr;
		llvm::GlobalVariable *compareFlag;

		llvm::Function *mainFunction;
		::std::vector<llvm::BasicBlock *> instructionsBB;
		size_t currentIP;

		::std::unique_ptr<llvm::Module> currentModule;
		codeReader *cr;

		typedef ::std::vector<llvm::Value *> functionArguments;

	public:
		nativeBackend(::std::string moduleName);
		~nativeBackend();

		void generateInitial();
		void generateDeinitial();

		void setCodeReader(codeReader *newCr);
		void generateCode();
		void generateInstructionCode(instruction inst);
		void dump() const;
		void write(llvm::raw_ostream &out) const;

		friend class jitCompiler;

	protected:
		llvm::Function *getModuleFunction(std::string name);

		llvm::Value *getInt(long long val);
		llvm::Value *getDouble(double val);

		llvm::Value *get(arg data);
		llvm::Value *getPtr(arg data);
		llvm::BasicBlock *getInstruction(size_t id);

		void generateMov(LLCCEP::instruction info);
		void generateMav(LLCCEP::instruction info);
		void generateMva(LLCCEP::instruction info);
		void generatePush(LLCCEP::instruction info);
		void generatePop(LLCCEP::instruction info);
		void generateTop(LLCCEP::instruction info);
		void generateAdd(LLCCEP::instruction info);
		void generateSub(LLCCEP::instruction info);
		void generateMul(LLCCEP::instruction info);
		void generateDiv(LLCCEP::instruction info);
		void generateAnd(LLCCEP::instruction info);
		void generateOr(LLCCEP::instruction info);
		void generateXor(LLCCEP::instruction info);
		void generateOff(LLCCEP::instruction info);
		void generateNop(LLCCEP::instruction info);
		void generateSwi(LLCCEP::instruction info);
		void generateCmp(LLCCEP::instruction info);
		void generateInc(LLCCEP::instruction info);
		void generateDec(LLCCEP::instruction info);
		void generateSqrt(LLCCEP::instruction info);
		void generateSin(LLCCEP::instruction info);
		void generateCos(LLCCEP::instruction info);
		void generatePtan(LLCCEP::instruction info);
		void generatePatan(LLCCEP::instruction info);
		void generateLdc(LLCCEP::instruction info);
		void generateCall(LLCCEP::instruction info);
		void generateJmp(LLCCEP::instruction info);
		void generateRet(LLCCEP::instruction info);
		void generateStregs(LLCCEP::instruction info);
		void generateLdregs(LLCCEP::instruction info);
	};
} // namespace LLCCEP
#endif /* LLCCEP_NATIVE_BACKEND_HPP */
