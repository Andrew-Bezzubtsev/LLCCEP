#include <llvm-support/error.h>

#include <stddef.h>

size_t heap_size;
double *heap;

#define REALLOCATE_HEAP(new_max_id) \
{ \
	size_t _new_max_id = (new_max_id); \
	if (_new_max_id >= heap_size) { \
		heap_size = _new_max_id + 1; \
		heap = realloc(heap, heap_size * sizeof(double)); \
		\
		if (!heap) \
			LLCCEP_LLVM_ERROR("cannot reallocate heap"); \
	} \
}

void heap_init(void)
{
	heap_size = 0;
	heap = NULL;
}

double heap_get(size_t index)
{
	REALLOCATE_HEAP(index);
	return heap[index];
}

void heap_put(size_t index, double value)
{
	REALLOCATE_HEAP(index);
	heap[index] = value;
}

void heap_delete(void)
{
	if (heap)
		free(heap);
}
