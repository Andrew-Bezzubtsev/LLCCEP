#ifndef LLCCEP_LLVM_SUPPORT_STACK_H
#define LLCCEP_LLVM_SUPPORT_STACK_H

#include <stddef.h>

struct stack {
	double *mem;
	size_t sz;
};

void stack_init(struct stack *stk);
void stack_delete(struct stack *stk);

void stack_push(struct stack *stk, double val);
void stack_pop(struct stack *stk);
double stack_top(struct stack *stk);
double stack_top_pop(struct stack *stk);

#endif /* LLCCEP_LLVM_SUPPORT_STACK_H */
