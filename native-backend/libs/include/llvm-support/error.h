#ifndef LLCCEP_LLVM_SUPPORT_ERROR_H
#define LLCCEP_LLVM_SUPPORT_ERROR_H

#include <stdio.h>
#include <stdlib.h>

#define LLCCEP_LLVM_ERROR(fmt, ...) \
{ \
	fprintf(stderr, "%s(): " fmt, __func__, ##__VA_ARGS__); \
	exit(EXIT_FAILURE); \
}

#endif /* LLCCEP_LLVM_SUPPORT_ERROR_H */
