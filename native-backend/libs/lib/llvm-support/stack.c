#include <llvm-support/error.h>
#include <llvm-support/stack.h>

#include <stddef.h>

#define VALIDATE_STACK(stk)                                         \
	{                                                           \
		if (!stk)                                           \
			LLCCEP_LLVM_ERROR("invalid stack pointer"); \
	}

#define REALLOCATE_STACK(stk, size)                                                          \
	{                                                                                    \
		struct stack *to_realloc = (stk);                                            \
		VALIDATE_STACK(to_realloc);                                                  \
		to_realloc->sz = (size);                                                     \
		to_realloc->mem = realloc(to_realloc->mem, sizeof(double) * to_realloc->sz); \
	}

void stack_init(struct stack *stk)
{
	VALIDATE_STACK(stk);

	stk->mem = NULL;
	stk->sz = 0;
}

void stack_delete(struct stack *stk)
{
	VALIDATE_STACK(stk);

	if (stk->mem) {
		free(stk->mem);
		stk->mem = NULL;
	}

	stk->sz = 0;
}

void stack_push(struct stack *stk, double val)
{
	VALIDATE_STACK(stk);

	REALLOCATE_STACK(stk, stk->sz++);
	stk->mem[stk->sz - 1] = val;
}

void stack_pop(struct stack *stk)
{
	VALIDATE_STACK(stk);

	if (!stk->sz)
		LLCCEP_LLVM_ERROR("nothing to pop");

	REALLOCATE_STACK(stk, stk->sz--);
}

double stack_top(struct stack *stk)
{
	VALIDATE_STACK(stk);

	if (!stk->sz)
		LLCCEP_LLVM_ERROR("no top to get");

	return stk->mem[stk->sz - 1];
}

#undef VALIDATE_STACK
#undef REALLOCATE_STACK
