#include <stdlib.h>
#include <string.h>

#include "ram.h"

double heap_get(size_t index);
void heap_put(size_t index, double value);

char *ram_read_string(size_t ptr)
{
	char *buf = NULL;
	size_t l = 0;

	do {
		buf = realloc(buf, l + 1);
		buf[l] = (char)heap_get(ptr + l);
		l++;
	} while (buf[l - 1]);

	return buf;
}

void ram_put_string(size_t ptr, const char *buf)
{
	size_t l = strlen(buf);
	for (size_t i = 0; i < l; i++)
		heap_put(ptr + i, (double)buf[i]);
}
