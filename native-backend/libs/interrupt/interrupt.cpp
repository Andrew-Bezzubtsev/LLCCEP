#include "interrupt.h"
#include "../../../emulator/window/window.hpp"

#include <LLCCEP/STDExtras.hpp>

#include <vector>

#define VERIFY_WINDOW(id) \
{ \
	if (id >= mangled_windows.size() || mangled_windows[id]) \
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG("Invalid window descriptor in interrupt")); \
}

using window = LLCCEP_exec::window;

static std::vector<window *> mangled_windows;

size_t create_window(int width, int height, const char *name)
{
	window *wnd = new window;
	wnd->resize(width, height);
	wnd->show();
	wnd->setWindowTitle(name);
	wnd->begin(width, height);
	wnd->setAntialiased(true);

	mangled_windows.push_back(wnd);
	return mangled_windows.size() - 1;
}

void draw_point(size_t id, int x, int y)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	wnd->painter().drawPoint(x, y);
}

void set_color(size_t id, uint32_t clr)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	wnd->painter().setPen(QColor(static_cast<QRgb>(clr)));
}

void close_window(size_t id)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	wnd->close();
	delete wnd;

	mangled_windows[id] = NULL;
}

#define MIN(a, b) \
({ \
	auto _a = (a); \
	auto _b = (b); \
	(_a < _b)?_a:_b; \
})

int get_kb_button(size_t id, size_t button)
{
	VERIFY_WINDOW(id);
	
	window *wnd = mangled_windows[id];
	return wnd->getKeyboardButtonState(MIN(button, (unsigned)0xFF));
}

int get_mouse_x(size_t id)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	return wnd->getMousePos().x();
}

int get_mouse_y(size_t id)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	return wnd->getMousePos().y();
}

int get_mouse_buttons(size_t id)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	return wnd->getMouseButtons();
}

int get_x(size_t id)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	return wnd->pos().x();
}

int get_y(size_t id)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	return wnd->pos().y();
}

int get_width(size_t id)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	return wnd->size().width();
}

int get_height(size_t id)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	return wnd->size().height();
}

void resize(size_t id, int width, int height)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	wnd->resize(width, height);
}

void move(size_t id, int width, int height)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	wnd->move(width, height);
}

void set_title(size_t id, const char *name)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	wnd->setWindowTitle(name);
}

void set_flags(size_t id, int flags)
{
	VERIFY_WINDOW(id);

	window *wnd = mangled_windows[id];
	wnd->setWindowFlags((Qt::WindowType)flags);
}
