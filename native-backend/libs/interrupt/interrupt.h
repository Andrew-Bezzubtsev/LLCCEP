#ifndef LLCCEP_LLVM_INTERRUPT_H
#define LLCCEP_LLVM_INTERRUPT_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

size_t create_window(int width, int height, const char *name);
void draw_point(size_t id, int x, int y);
void set_color(size_t id, uint32_t clr);
void close_window(size_t id);
int get_kb_button(size_t id, size_t button);
int get_mouse_x(size_t id);
int get_mouse_y(size_t id);
int get_mouse_buttons(size_t id);
int get_x(size_t id);
int get_y(size_t id);
int get_width(size_t id);
int get_height(size_t id);
void resize(size_t id, int width, int height);
void move(size_t id, int x, int y);
void set_title(size_t id, const char *name);
void set_flags(size_t id, int flags);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* LLCCEP_LLVM_INTERRUPT_H */
