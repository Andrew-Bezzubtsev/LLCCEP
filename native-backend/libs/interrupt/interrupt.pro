QMAKE_CFLAGS += -std=gnu11 -c -S -emit-llvm
QMAKE_CXXFLAGS += -std=gnu++11 -c -S -emit-llvm

QT += widgets

INCLUDEPATH += ../../../include/
INCLUDEPATH += ../include/

SOURCES = interrupt.cpp \
interrupt_host.c \
ram.c \
../../../emulator/window/window.cpp \
../../../emulator/window/renderer/renderer.cpp \
../../../emulator/messageBox/messageBox.cpp \
../../../lib/STDExtras.cpp

HEADERS = interrupt.h \
ram.h \
../../../emulator/window/window.hpp \
../../../emulator/window/renderer/renderer.hpp \
../../../emulator/messageBox/messageBox.hpp \ 
