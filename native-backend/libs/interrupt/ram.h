#ifndef LLCCEP_LLVM_RAM_H
#define LLCCEP_LLVM_RAM_H

char *ram_read_string(size_t ptr);
void ram_put_string(size_t ptr, const char *buf);

#endif /* LLCCEP_LLVM_RAM_H */
