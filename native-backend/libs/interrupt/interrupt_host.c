#include <llvm-support/registers.h>
#include <llvm-support/error.h>

#include <stdio.h>
#include <stdlib.h>

#include "interrupt.h"
#include "ram.h"

#define NUMBER_FMT  "%lg"
#define CHAR_FMT    "%c"
#define STRING_FMT  "%s"
#define HEX_FMT     "%x"
#define OCT_FMT     "%o"
#define INVALID_FMT ""

#define ARRAY_LENGTH(arr) (sizeof(arr) / sizeof(*arr))

static inline void interrupt_get_std_file(void)
{
	FILE *std_files[3] = {
		stdin,
		stdout,
		stderr
	};

	int id = (int)registers[1];
	if (id < 0 || id > 2)
		LLCCEP_LLVM_ERROR("invalid std file id to get");

	(*(FILE **)&registers[2]) = std_files[id];
}

static inline void interrupt_write(void)
{
	int fmt = (int)registers[2];
	FILE *out = *(FILE **)&registers[1];
	char *buf;

	switch (fmt) {
	case 'n':
		fprintf(out, NUMBER_FMT, registers[3]);
		break;

	case 'c':
		fprintf(out, CHAR_FMT, (char)registers[3]);
		break;

	case 's':
		buf = ram_read_string((size_t)registers[3]);
		fprintf(out, STRING_FMT, buf);
		free(buf);
		break;

	case 'x':
		fprintf(out, HEX_FMT, (unsigned)registers[3]);
		break;

	case 'o':
		fprintf(out, OCT_FMT, (unsigned)registers[3]);
		break;

	default:
		LLCCEP_LLVM_ERROR("invalid format for output");
	}
}

static inline void interrupt_read(void)
{
	int fmt = (int)registers[2];
	FILE *in = *(FILE **)&registers[1];
	size_t junk;

	union {
		char chr;
		char *buf;
		unsigned value;
	} temp;

	switch (fmt) {
	case 'n':
		fscanf(in, NUMBER_FMT, &registers[3]);
		break;

	case 'c':
		fscanf(in, CHAR_FMT, &temp.chr);
		registers[3] = (double)temp.chr;
		break;

	case 's':
		getline(&temp.buf, &junk, in);
		ram_put_string((size_t)registers[3], temp.buf);
		free(temp.buf);
		break;

	case 'x':
		fscanf(in, HEX_FMT, &temp.value);
		registers[3] = (double)temp.value;
		break;

	case 'o':
		fscanf(in, OCT_FMT, &temp.value);
		registers[3] = (double)temp.value;
		break;

	default:
		LLCCEP_LLVM_ERROR("invalid format for input");
	}
}

static inline void interrupt_fopen(void)
{
	char *path = ram_read_string((size_t)registers[2]);
	char *mode = ram_read_string((size_t)registers[3]);

	FILE *f = fopen(path, mode);

	free(path);
	free(mode);

	(*(FILE **)&registers[1]) = f;
}

static inline void interrupt_fclose(void)
{
	FILE *f = *(FILE **)&registers[1];
	fclose(f);
}

static inline void interrupt_file(int func)
{
	void (*functions[])(void) = {
		interrupt_get_std_file,
		interrupt_write,
		interrupt_read,
		interrupt_fopen,
		interrupt_fclose
	};

	int sz = ARRAY_LENGTH(functions);
	if (func < 0 || func <= sz)
		LLCCEP_LLVM_ERROR("invalid interrupt function");

	functions[func]();
}

static inline void interrupt_window_create(void)
{
	int w = (int)registers[1];
	int h = (int)registers[2];
	char *name = ram_read_string((size_t)registers[3]);

	registers[4] = (double)create_window(w, h, name);

	free(name);
}

static inline void interrupt_window_pix(void)
{
	size_t id = (size_t)registers[1];
	int x = (int)registers[2];
	int y = (int)registers[3];

	draw_point(id, x, y);
}

static inline void interrupt_window_clr(void)
{
	size_t id = (size_t)registers[1];
	uint32_t clr = (uint32_t)registers[2];

	set_color(id, clr);
}

static inline void interrupt_window_close(void)
{
	size_t id = (size_t)registers[1];
	close_window(id);
}

static inline void interrupt_get_mouse(void)
{
	size_t id = (size_t)registers[1];
	registers[2] = (double)get_mouse_x(id);
	registers[3] = (double)get_mouse_y(id);
}

static inline void interrupt_get_kb(void)
{
	size_t id = (size_t)registers[1];
	uint8_t but = (uint8_t)registers[2];

	registers[3] = (double)get_kb_button(id, but);
}

static inline void interrupt_get_window_pos(void)
{
	size_t id = (size_t)registers[1];
	registers[2] = (double)get_x(id);
	registers[3] = (double)get_y(id);
}

static inline void interrupt_get_window_metrics(void)
{
	size_t id = (size_t)registers[1];
	registers[2] = (double)get_width(id);
	registers[3] = (double)get_height(id);
}

static inline void interrupt_window_resize(void)
{
	size_t id = (size_t)registers[1];
	int w = (int)registers[2];
	int h = (int)registers[3];

	resize(id, w, h);
}

static inline void interrupt_window_move(void)
{
	size_t id = (size_t)registers[1];
	int x = (int)registers[2];
	int y = (int)registers[3];
	
	move(id, x, y);
}

static inline void interrupt_window_set_title(void)
{
	size_t id = (size_t)registers[1];
	char *name = ram_read_string((size_t)registers[2]);

	set_title(id, name);
	free(name);
}

static inline void interrupt_window_set_flags(void)
{
	size_t id = (size_t)registers[1];
	int flags = (int)registers[2];

	set_flags(id, flags);
}

static inline void interrupt_window(int func)
{
	void (*functions[])(void) = {
		interrupt_window_create,
		interrupt_window_pix,
		interrupt_window_clr,
		interrupt_window_close,
		interrupt_get_mouse,
		interrupt_get_kb,
		interrupt_get_window_pos,
		interrupt_get_window_metrics,
		interrupt_window_resize,
		interrupt_window_move,
		interrupt_window_set_title,
		interrupt_window_set_flags
	};

	int sz = ARRAY_LENGTH(functions);
	if (func < 0 || func >= sz)
		LLCCEP_LLVM_ERROR("invalid interrupt function");

	functions[func]();
}

void interrupt(int id)
{
	int function = (int)registers[0];

	switch (id) {
	case 0:
		interrupt_file(function);
		break;

	case 1:
		interrupt_window(function);
		break;

	default:
		LLCCEP_LLVM_ERROR("invalid interrupt id");
	}
}


