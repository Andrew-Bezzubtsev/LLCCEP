#include <llvm-support/stack.h>

static struct stack stack;

void machine_stack_init(void)
{
	stack_init(&stack);
}

void machine_stack_delete(void)
{
	stack_delete(&stack);
}

void machine_stack_push(double val)
{
	stack_push(&stack, val);
}

double machine_stack_top(void)
{
	return stack_top(&stack);
}

void machine_stack_pop(void)
{
	stack_pop(&stack);
}
