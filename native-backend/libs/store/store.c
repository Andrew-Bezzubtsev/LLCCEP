#include <llvm-support/stack.h>
#include <llvm-support/registers.h>

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

static struct stack registers_storage;

void registers_storage_init(void)
{
	stack_init(&registers_storage);
}

void registers_store(void)
{
	double *mem = calloc(LLCCEP_LLVM_REGNO, sizeof(LLCCEP_LLVM_REG_T));
	memcpy(mem, registers, LLCCEP_LLVM_REGNO * sizeof(LLCCEP_LLVM_REG_T));
	stack_push(&registers_storage, *(double *)&mem);
}

void registers_load(void)
{
	if (!registers_storage.sz)
		raise(SIGSEGV);

	double value = stack_top(&registers_storage);
	double *old_snapshot = *(double **)&value;
	stack_pop(&registers_storage);

	memcpy(registers, old_snapshot, LLCCEP_LLVM_REGNO * sizeof(LLCCEP_LLVM_REG_T));
	free(old_snapshot);
}

void registers_storage_free(void)
{
	stack_delete(&registers_storage);
}
