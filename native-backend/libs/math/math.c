#include <math.h>

typedef long long unsigned i64;

#define MIN(i, j) ((i < j)?(i):(j))

double loadConstant(i64 id)
{
	double constants[] = {
		1.0,
		3.32192809489,
		M_LOG2E,
		M_PI,
		0.30102999566,
		M_LN2,
		0,
		-1
	};

	return constants[MIN(id, 7)];
}
