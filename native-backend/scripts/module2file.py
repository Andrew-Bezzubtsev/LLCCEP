#!/usr/bin/env python

import sys

def generateCode(srcCode):
	return ('"' + srcCode.replace('\\', '\\\\').replace('%', '\%').replace('"', '\\"').replace('\'', '\\\'').replace('\n', '\\n"\n"') + '"')

def generateNewFile(srcCode, outName, outPath):
	o = open(outPath, "w")
	code = generateCode(srcCode).split('\n')
	code = code[0:len(code) - 1]

	macro = 'LLCCEP_NATIVE_BACKEND_' + outName.upper() + '_HPP'
	o.write('#ifndef ' + macro + '\n' +
		'#define ' + macro + '\n' +
		'\n' +
		'#include <string>\n' +
		'\n'
		'namespace LLCCEP {\n' +
		'\tconst ::std::string ' + outName + 'Module =');

	for i in code:
		o.write('\n\t\t' + i)

	o.write(';\n}\n\n#endif /* ' + macro + ' */')
	o.close()

if len(sys.argv) != 4:
	print('Usage: ' + sys.argv[0] + ' <Input LLVM module> <Output module name> <Output file>\n')
	sys.exit(0)

llvmModuleContent = open(sys.argv[1], "r")
moduleText = llvmModuleContent.read()
llvmModuleContent.close()

generateNewFile(moduleText, sys.argv[2], sys.argv[3])
