#!/usr/bin/env python

import sys
if sys.version_info[0] < 3 or sys.version_info[1] < 4:
	print("This script requires Python 3.4 or higher")
	sys.exit(0)

import os
import pathlib

modules2files = [
	[ 'libs/host.ll', 'host' ]
]

def getMyDirectory():
	return os.path.dirname(os.path.abspath(__file__))

def getRunRoot():
	return str(pathlib.Path(getMyDirectory()).parent) + '/'

def hasDirectory(dirname):
	return os.path.isdir(dirname) and os.path.exists(dirname)

def ensureOutdirectory():
	directory = getRunRoot() + '/llvmModules/'

	if not hasDirectory(directory):
		if os.path.exists(directory):
			os.remove(directory)

		os.mkdir(directory)

	return directory

outDir = ensureOutdirectory()
script = getMyDirectory() + '/module2file.py'

for i in modules2files:
	outFile = outDir + i[1] + '.hpp'
	os.system('python ' + script + ' ' + getRunRoot() + i[0] + ' ' + 
	          i[1] + ' ' + outFile)
