#include <cstdio>

#include <llvm/Support/raw_ostream.h>

#include <LLCCEP/codeReader.hpp>
#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/command-line.hpp>

#include "backend/backend.hpp"

int main(int argn, char **argv)
{
	try {
		LLCCEP_tools::commandLineParametersParser clpp;
		clpp.addFlag(LLCCEP_tools::commandLineFlag{{"-h", "--help"},
		                                           "help", false});
		clpp.addFlag(LLCCEP_tools::commandLineFlag{{"-o", "--output"},
		                                           "output", true});
		clpp.setHelpText("LLCCEP to LLVM IR Compiler help.\n"
				 "-h/--help -- show help(this text)\n"
				 "-o/--output -- set output file(a path is after)[a.ll by default]");
		clpp.setMaxFreeParams(1);
		clpp.parse(argn, argv);

		if (clpp.getParam("help") == "1" ||
		    !clpp.getFreeParams().size()) {
			if (!clpp.getFreeParams().size())
				::std::fprintf(stderr, "No input!\n");

			clpp.showHelp();
			return 0;
		}

		::std::FILE *in;
		OPEN_FILE(in, clpp.getFreeParams()[0].c_str(), "r");

		LLCCEP::codeReader cr(in);
		LLCCEP::nativeBackend nb(clpp.getFreeParams()[0].c_str());

		nb.setCodeReader(&cr);
		nb.generateCode();

		::std::string tmp;
		llvm::raw_string_ostream tmpStream(tmp);
		nb.write(tmpStream);

		::std::FILE *out;
		::std::string outPath = (clpp.getParam("output").length()?clpp.getParam("output"):"a.ll");
		OPEN_FILE(out, outPath.c_str(), "w");
		::std::fprintf(out, "%s", tmp.c_str());
	} DEFAULT_HANDLING

	return 0;
}
