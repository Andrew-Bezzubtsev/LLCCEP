#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>

#include <llvm/IRReader/IRReader.h>

#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>

#include <LLCCEP/STDExtras.hpp>

#include <memory>

#include "moduleReader.hpp"

::std::unique_ptr<llvm::Module> LLCCEP::loadLLVMModule(llvm::LLVMContext &cxt, ::std::string path)
{
	llvm::SMDiagnostic err;
	auto mod = llvm::parseIRFile(path, err, cxt);
	if (!mod) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Cannot compile outer LLVM module!\n"
			"%s:%d: %s",
			err.getFilename().str().c_str(),
			err.getLineNo(),
			err.getMessage().str().c_str()));
	}

	return mod;
}

::std::unique_ptr<llvm::Module> LLCCEP::loadLLVMModule(llvm::LLVMContext &cxt, ::std::string name, ::std::string modText)
{
	llvm::StringRef strBuf(modText);
	llvm::MemoryBufferRef buf(strBuf, name);
	llvm::SMDiagnostic err;

	auto mod = llvm::parseIR(buf, err, cxt);
	if (!mod) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Cannot compile outer LLVM module!\n"
			"%s:%d: %s",
			err.getFilename().str().c_str(),
			err.getLineNo(),
			err.getMessage().str().c_str()));
	}

	return mod;
}

