#include <cstdio>
#include <cerrno>

#include "moduleReader.hpp"
#include "./../llvmModules/stack.hpp"

int main(int argn, char **argv) {
	if (argn != 2) {
		::std::fprintf(stderr, "No input!");
		return EINVAL;
	}

//	LLCCEP::loadLLVMModule(argv[1])->dump();
	LLCCEP::loadLLVMModule("stackModule", LLCCEP::stackModule)->dump();

	return 0;
}
