#ifndef LLCCEP_NATIVE_BACKEND_MODULE_READER_HPP
#define LLCCEP_NATIVE_BACKEND_MODULE_READER_HPP

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>

#include <llvm/IRReader/IRReader.h>

#include <llvm/Support/SourceMgr.h>
#include <llvm/Support/raw_ostream.h>

#include <LLCCEP/STDExtras.hpp>

#include <memory>

namespace LLCCEP {
	::std::unique_ptr<llvm::Module> loadLLVMModule(llvm::LLVMContext &cxt, ::std::string name, ::std::string modText);
	::std::unique_ptr<llvm::Module> loadLLVMModule(llvm::LLVMContext &cxt, ::std::string path);
}

#endif /* LLCCEP_NATIVE_BACKEND_MODULE_READER_HPP */
