QT += widgets multimedia

INCLUDEPATH += ./../include/
CONFIG += gnu++14 release

QMAKE_CXXFLAGS_DEBUG += -Wall -g -O0
QMAKE_CXXFLAGS_RELEASE += -Wall -Ofast \
                          -march=native \
                          -funroll-loops \

SOURCES = window/window.cpp \
          window/renderer/renderer.cpp \
          softcore/softcore.cpp \
          mm/mm.cpp \
          signal/signal.cpp \
          ../lib/STDExtras.cpp \
          ../lib/command-line.cpp \
	  ../lib/codeReader.cpp \
          ../common/def/def_inst.cpp \
          ../common/def/def_cond.cpp \
	  main.cpp \
          messageBox/messageBox.cpp

HEADERS = window/window.hpp \
          window/renderer/renderer.hpp \
          softcore/softcore.hpp \
          softcore/fp.hpp \
          mm/mm.hpp \
          signal/signal.hpp \
          ../common/def/def_inst.hpp \
          ../common/def/def_cond.hpp \
          messageBox/messageBox.hpp

TARGET  = "LLCCEP Emulator"
