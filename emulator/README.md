# Emulator(VM) of LLCCEP

## About
This program interprets programs, translated into LLCCEP VM bytecode via
LLCCEP assembler.

## Building the VM
Build dependencies: C++ compiler, supporting GNU++14 language
standard, QMake utility, Qt Framework, Qt Creator/Make.

* Just open project in Qt Creator and build it
* Generate Makefile and compile emulator via Make

Generate Makefile and compile emulator:

    qmake -makefile emulator.pro;
    make;

The build was tested on the Clang compiler and Qt Creator 5.7. Building via GCC/MinGW also OK, but is not frequently tested, as I prefer using Clang.

## Command-line parameters
The VM is intended for command-line usage. It has got some specific options, 
to make its usage more flexible.<br>
Command-line parameters(options):
* --help/-h -- show help text
* The single free parameter will become input program

## Remarks
* RAM for VM is being dynamically reallocted by necessity 
