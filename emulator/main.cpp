#include <QApplication>

#include <string>
#include <cstdio>
#include <cstring>

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/command-line.hpp>
#include <LLCCEP/codeReader.hpp>

#include "messageBox/messageBox.hpp"
#include "softcore/softcore.hpp"
#include "mm/mm.hpp"
#include "signal/signal.hpp"

int main(int argn, char **argv)
{
	/* Initialize QT application */
	QApplication app(argn, argv);

	/* Handle signals, sent by program by
	 * telling about them to user and
	 * ending the execution
	 */
	LLCCEP_exec::cAttachSignalsHandler();

	try {
		/* Windows, created by the executing program */
		::std::vector<LLCCEP_exec::window *> windows;
		;
		/* Command-line parameters */
		LLCCEP_tools::commandLineParametersParser clpp;
		clpp.addFlag(LLCCEP_tools::commandLineFlag{{"--help", "-h"},
							   "help", false});
		clpp.setHelpText("LLCCEP debugger help:\n"
				 "-h/--help    | show help(this text)\n"
				 "The single free parameter will be input "
				 "program");
		clpp.setMaxFreeParams(1);
		/* Parse command-line parameters */
		clpp.parse(argn, argv);
		/* If needed to show help, show it
		 * and end the execution
		 */
		if (clpp.getParam("help") == "1" ||
		    !clpp.getFreeParams().size()) {
			if (!clpp.getFreeParams().size())
				::std::cerr << "No input!" << ::std::endl;

			clpp.showHelp();
			return 0;
		}

		/* Soft-processor */
		LLCCEP_exec::softcore sc;
		/* Memeory manager */
		LLCCEP_exec::memoryManager mm;
		/* Program reader */
		LLCCEP::codeReader cr;

		::std::FILE *in;
		OPEN_FILE(in, clpp.getFreeParams()[0].c_str(), "r");

		/* Read program's common information */
		cr.initializeInputFile(in);
		cr.readProgramHeader();

		/* Initialize soft-processor data */
		sc.setMm(&mm);
		sc.setCodeReader(&cr);

		/* Execute program */
		sc.executeProgram();

		/* Close input */
		::std::fclose(in);
	} catch (LLCCEP::runtime_exception &exc) {
		/* Spawn message box in case of
		 * internal exception
		 */
		LLCCEP_exec::messageBox("Program was interrupted by an exception",
					exc.msg(),
					QMessageBox::Close,
					QMessageBox::Close,
					QMessageBox::Critical).spawn();
		/* Dump to console error information */
		QUITE_ERROR(yes, "Program was interrupted by an exception:\n"
				 "%s", exc.msg());
	} catch (::std::exception &exc) {
		/* Spawn message box in case of
		 * internal exception
		 */
		LLCCEP_exec::messageBox("Program was interrupted by an exception",
					exc.what(),
					QMessageBox::Close,
					QMessageBox::Close,
					QMessageBox::Critical).spawn();
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "Program was interrupted by an exception:\n"
				 "%s", exc.what());
	} catch (::std::string &err) {
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "%s", err.c_str());
	} catch (int64_t id) {
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "Error " int64_t_pf, id);
	} catch (...) {
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "Unknown exception");
	}

	return 0;
}
