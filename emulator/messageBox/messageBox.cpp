#include <QMessageBox>
#include "messageBox.hpp"

LLCCEP_exec::messageBox::messageBox(::std::string title,
				    ::std::string subtitle,
				    StandardButtons buttons,
				    StandardButton selected,
				    Icon icon):
	QMessageBox(icon, QString(title.c_str()), QString(subtitle.c_str()),
		    buttons)
{
	setDefaultButton(selected);
}

int LLCCEP_exec::messageBox::spawn()
{
	return exec();
}
