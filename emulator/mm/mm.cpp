#include <cstddef>
#include <cstdint>
#include <new>
#include <cstring>
#include <LLCCEP/STDExtras.hpp>

#include "mm.hpp"

#define MM_CHECK_BLOCK(cond) DEFAULT_CHECK_BLOCK(cond, this, OK())
#define MM_OK_BLOCK MM_CHECK_BLOCK(true)
#define MM_NOTOK_BLOCK MM_CHECK_BLOCK(false)

LLCCEP_exec::memoryManager::memoryManager():
	mem(NULL),
	actualSz(0)
{ }

LLCCEP_exec::memoryManager::~memoryManager()
{
	freeElements();
}

void LLCCEP_exec::memoryManager::freeElements()
{
	free((void *)mem);
	mem = NULL;
	actualSz = 0;
}

::std::string LLCCEP_exec::memoryManager::getString(size_t offset)
{
	MM_OK_BLOCK;

	::std::string res;
	for (size_t i = offset; at(i); i++)
		res += static_cast<uint8_t>(at(i));

	MM_OK_BLOCK;

	return res;
}

void LLCCEP_exec::memoryManager::writeString(size_t offset, ::std::string str)
{
	MM_OK_BLOCK;

	for (size_t i = offset; i - offset < str.length(); i++)
		at(i) = static_cast<double>(str[i - offset]);

	MM_OK_BLOCK;
}

void *LLCCEP_exec::memoryManager::getMemBeginning()
{
	MM_OK_BLOCK;

	return reinterpret_cast<void *>(addr(0));
}

double &LLCCEP_exec::memoryManager::operator[](size_t id)
{
	MM_OK_BLOCK;

	return at(id);
}

bool LLCCEP_exec::memoryManager::OK() const
{
	return true;
}

double &LLCCEP_exec::memoryManager::at(size_t id)
{
	MM_OK_BLOCK;

	double *ptr = addr(id);

	MM_OK_BLOCK;

	return *ptr;
}

double *LLCCEP_exec::memoryManager::addr(size_t id)
{
	MM_OK_BLOCK;

	if (id >= actualSz) {
		/* Allocate new cells */
		mem = (double *)realloc(mem, sizeof(double) * (actualSz = id + 1));
		/* If allocation failed */
		if (!mem) {
			/* Throw exception about it */
			throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
				"Failed to reallocate memory for memoryManager: %s",
				::std::extras::strerror_safe(errno).c_str()));
		}
	}

	MM_OK_BLOCK;

	/* Return required cell pointer */
	return mem + id * sizeof(double);
}

#undef MM_CHECK_BLOCK
#undef MM_OK_BLOCK
#undef MM_NOTOK_BLOCK
