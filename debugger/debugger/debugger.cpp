#include <iostream>
#include <map>
#include <vector>
#include <regex>
#include <string>

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/STLExtras.hpp>
#include <LLCCEP/convert.hpp>

#include "../debugCore/debugCore.hpp"

#include "debugger.hpp"

#define DEBUGGER_CHECK_BLOCK(cond) DEFAULT_CHECK_BLOCK(cond, this, OK());
#define DEBUGGER_OK DEBUGGER_CHECK_BLOCK(true);
#define DEBUGGER_NOT_OK DEBUGGER_CHECK_BLOCK(false);

LLCCEP_debugger::debugger::debugger():
	attachedCore(NULL)
{ }

LLCCEP_debugger::debugger::~debugger()
{ }

void LLCCEP_debugger::debugger::attachCore(LLCCEP_debugger::debugCore *core)
{
	DEBUGGER_NOT_OK;

	/* If given debug core
	 * is invalid
	 */
	if (!core) {
		/* Throw exception about it */
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid core to be attached to debugger"));
	}

	/* Set debugger's core to a
	 * given one
	 */
	attachedCore = core;

	DEBUGGER_OK;
}

void LLCCEP_debugger::debugger::runDebugProcess()
{
	DEBUGGER_OK;

	/* While exit was not required and
	 * debug core is still running
	 */
	while (!quited && attachedCore->running() && !::std::cin.eof()) {
		/* Read user's input
		 * and execute the command
		 */
		executeUserCommand();
	}

	DEBUGGER_OK;
}

void LLCCEP_debugger::debugger::executeUserCommand()
{
	DEBUGGER_OK;

	::std::string cmd;
	bool gotCommand = false;

	while (!gotCommand && !::std::cin.eof()) {
		cmd.clear();
		::std::cout << "$> ";
		/* Read next user line */
		::std::getline(::std::cin, cmd);
		/* Split got line into tokens */
		auto tokens = tokenizer::split(cmd, " ", ".+");

		/* If there are no tokens */
		if (!tokens.size()) {
			::std::cout << "No input!" << ::std::endl;
			/* Redo input */
			continue;
		}

		auto found = BINDINGS.find(tokens[0]);
		/* If the first is not debugger command */
		if (found == BINDINGS.end()) {
			/* Tell about invalid command */
			::std::cout << "'" << tokens[0] << "'" " is not a valid debugger command"
			            << ::std::endl;
			/* Redo input */
			continue;
		}

		/* Create command */
		debuggerCommand cmd{};
		/* Get command type */
		cmd.type = found->second;
		/* Emit program arguments */
		cmd.args.insert(cmd.args.begin(), tokens.begin() + 1,
				tokens.end());
		/* Execute got command */
		if (runCommand(cmd))
			gotCommand = true;
	}

	DEBUGGER_OK;
}

bool LLCCEP_debugger::debugger::runCommand(LLCCEP_debugger::debugger::debuggerCommand cmd)
{
	DEBUGGER_OK;

	/* If given command
	 * is invalid
	 */
	if (!validateCommand(cmd)) {
		/* Tell to debugger, that it
		 * is impossible to execute
		 * it
		 */
		return false;
	}

	switch (cmd.type) {
	/* Create a new breakpoint */
	case LLCCEP_debugger::debugger::DBGCMD_T_ADDBP:
		attachedCore->addBreakpoint(cmd.args[0], cmd.args[1], 
		                            from_string<size_t>(cmd.args[2]));
		break;

	/* Delete existing breakpoint */
	case LLCCEP_debugger::debugger::DBGCMD_T_DELBP:
		attachedCore->deleteBreakpoint(cmd.args[0]);
		break;

	/* List existing breakpoints */
	case LLCCEP_debugger::debugger::DBGCMD_T_LISTBP:
		attachedCore->breakpointDump(::std::cout);
		break;

	/* Continue running code */
	case LLCCEP_debugger::debugger::DBGCMD_T_RUN:
		attachedCore->continueExecution();
		break;

	/* Dump registers */
	case LLCCEP_debugger::debugger::DBGCMD_T_REGS:
		attachedCore->regDump(::std::cout);
		break;

	/* Dump RAM */
	case LLCCEP_debugger::debugger::DBGCMD_T_RAM:
		attachedCore->memDump(::std::cout, 
				      from_string<size_t>(cmd.args[0]),
				      from_string<size_t>(cmd.args[1]));
		break;

	/* Write a value to register */
	case LLCCEP_debugger::debugger::DBGCMD_T_REGWRITE:
		attachedCore->regWrite(from_string<size_t>(cmd.args[0]),
		                       from_string<double>(cmd.args[1]));
		break;

	/* Write a value to RAM cell */
	case LLCCEP_debugger::debugger::DBGCMD_T_RAMWRITE:
		attachedCore->memWrite(from_string<size_t>(cmd.args[0]),
		                       from_string<double>(cmd.args[1]));
		break;

	/* Dump call stack */
	case LLCCEP_debugger::debugger::DBGCMD_T_CALLSTACK:
		attachedCore->callStackDump(::std::cout);
		break;

	/* Dump stack */
	case LLCCEP_debugger::debugger::DBGCMD_T_STACK:
		attachedCore->stackDump(::std::cout);
		break;

	/* Disable created breakpoint */
	case LLCCEP_debugger::debugger::DBGCMD_T_DISABLE:
		attachedCore->deactivateBreakpoint(cmd.args[0]);
		break;

	/* Enable created breakpoint */
	case LLCCEP_debugger::debugger::DBGCMD_T_ENABLE:
		attachedCore->activateBreakpoint(cmd.args[0]);
		break;

	/* Show debugger help */
	case LLCCEP_debugger::debugger::DBGCMD_T_HELP:
		::std::cout << HELP;
		break;

	/* Exit debugger */
	case LLCCEP_debugger::debugger::DBGCMD_T_QUIT:
		quited = true;
		break;

	/* Unknown command */
	default:
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid command"));
	}

	DEBUGGER_OK;

	/* Tell that execution
	 * ended OK
	 */
	return true;
}

bool LLCCEP_debugger::debugger::validateCommand(LLCCEP_debugger::debugger::debuggerCommand cmd)
{
	DEBUGGER_OK;

	/* Get amount of arguments
	 * to a command
	 */
	auto getArgno = [this](debuggerCommand_t type) {
		/* Place, where type is declared at table */
		auto key = TYPEDECL.find(type);
		/* If there is no
		 * such type
		 */
		if (key == TYPEDECL.end()) {
			/* Throw exception about it */
			throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
				"No '%d' token type", type));
		}

		/* Result */
		size_t res = 0;
		/* Pass through all argument types */
		for (const auto &i: key->second) {
			/* If reached end */
			if (i == DBGTOK_T_NONE) {
				/* Exit loop */
				break;
			/* Else */
			} else {
				/* Increment number of
				 * required arguments
				 */
				res++;
			}
		}

		return res;
	};

	auto typeMatch = [this](::std::string str, LLCCEP_debugger::debugger::debuggerToken_t tok_t) {
		auto found = TOKDECL.find(tok_t);
		if (found == TOKDECL.end()) {
			throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
				"Invalid token type to match type"));
		}

		::std::regex expression4type(found->second, ::std::regex_constants::egrep);
		return ::std::regex_match(str, expression4type);
	};

	/* Get amount of arguments to the command */
	size_t argno = getArgno(cmd.type);
	/* If it mismatches with the
	 * given one
	 */
	if (cmd.args.size() != argno) {
		/* Tell to user that cmd is invalid */
		::std::cerr << "Invalid arguments list -1" << ::std::endl;
		/* Tell to debugger that cmd is invalid */
		return false;
	}

	/* Go through arguments */
	for (size_t i = 0; i < argno; i++) {
		if (TYPEDECL.find(cmd.type) == TYPEDECL.end()) {
			throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
				"Invalid type"));
		}

		if (!typeMatch(cmd.args[i], TYPEDECL.at(cmd.type)[i])) {
			/* Tell to user that
			 * cmd is invalid
			 */
			::std::cerr << "Invalid arguments list " << i << ::std::endl;
			/* Tell to debugger that cmd
			 * is invalid 
			 */
			return false;
		}
	}

	DEBUGGER_OK;

	return true;
}

bool LLCCEP_debugger::debugger::OK() const
{
	return attachedCore && attachedCore->OK();
}
