#ifndef LLCCEP_DEBUGGER_DEBUGGER_HPP
#define LLCCEP_DEBUGGER_DEBUGGER_HPP

#include <map>
#include <vector>
#include <string>

#include <LLCCEP/STDExtras.hpp>

#include "../debugCore/debugCore.hpp"

namespace LLCCEP_debugger {
	class debugger {
		/* Make class uncopiable */
		UNCOPIABLE_CLASS(debugger);
	
		/* Commands of debugger */
		enum debuggerCommand_t {
			DBGCMD_T_ADDBP,
			DBGCMD_T_DELBP,
			DBGCMD_T_LISTBP,
			DBGCMD_T_RUN,
			DBGCMD_T_REGS,
			DBGCMD_T_RAM,
			DBGCMD_T_REGWRITE,
			DBGCMD_T_RAMWRITE,
			DBGCMD_T_CALLSTACK,
			DBGCMD_T_STACK,
			DBGCMD_T_DISABLE,
			DBGCMD_T_ENABLE,
			DBGCMD_T_HELP,
			DBGCMD_T_QUIT
		};

		/* To-command arguments tokens */
		enum debuggerToken_t {
			DBGTOK_T_ID,
			DBGTOK_T_INT,
			DBGTOK_T_REAL,
			DBGTOK_T_PATH,
			DBGTOK_T_NONE
		};
	
		/* Tokens declarations via regular expressions */
		const ::std::map<debuggerToken_t, ::std::string> TOKDECL = {
			{DBGTOK_T_ID,   "[a-zA-Z_][a-zA-Z0-9_]*"},
			{DBGTOK_T_INT,  "[0-9]+"},
			{DBGTOK_T_REAL, "[+-]?([0-9]+\\.)?[0-9]+"},
			{DBGTOK_T_PATH, ".*"},
			{DBGTOK_T_NONE, ""} /* No one regular expression matches it, aka empty string */
		};

		/* Max amount of parameters */
		const size_t PARAM_NO = 3;

		/* Declaration of required tokens types to debugger
		 * commands
		 */
		const ::std::map<debuggerCommand_t, ::std::vector<debuggerToken_t> > TYPEDECL = {
			{DBGCMD_T_ADDBP,     {DBGTOK_T_ID,   DBGTOK_T_PATH, DBGTOK_T_INT}},
			{DBGCMD_T_DELBP,     {DBGTOK_T_ID,   DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_LISTBP,    {DBGTOK_T_NONE, DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_RUN,       {DBGTOK_T_NONE, DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_REGS,      {DBGTOK_T_NONE, DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_RAM,       {DBGTOK_T_INT,  DBGTOK_T_INT,  DBGTOK_T_NONE}},
			{DBGCMD_T_REGWRITE,  {DBGTOK_T_INT,  DBGTOK_T_REAL, DBGTOK_T_NONE}},
			{DBGCMD_T_RAMWRITE,  {DBGTOK_T_INT,  DBGTOK_T_REAL, DBGTOK_T_NONE}},
			{DBGCMD_T_CALLSTACK, {DBGTOK_T_NONE, DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_STACK,     {DBGTOK_T_NONE, DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_DISABLE,   {DBGTOK_T_ID,   DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_ENABLE,    {DBGTOK_T_ID,   DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_HELP,      {DBGTOK_T_NONE, DBGTOK_T_NONE, DBGTOK_T_NONE}},
			{DBGCMD_T_QUIT,      {DBGTOK_T_NONE, DBGTOK_T_NONE, DBGTOK_T_NONE}}
		};

		/* Bindings of command mnemonics to
		 * their codes
		 */
		const ::std::map<::std::string, debuggerCommand_t> BINDINGS = {
			{"addbp",     DBGCMD_T_ADDBP},
			{"delbp",     DBGCMD_T_DELBP},
			{"listbp",    DBGCMD_T_LISTBP},
			{"run",       DBGCMD_T_RUN},
			{"regs",      DBGCMD_T_REGS},
			{"ram",       DBGCMD_T_RAM},
			{"regwrite",  DBGCMD_T_REGWRITE},
			{"ramwrite",  DBGCMD_T_RAMWRITE},
			{"call",      DBGCMD_T_CALLSTACK},
			{"stack",     DBGCMD_T_STACK},
			{"disable",   DBGCMD_T_DISABLE},
			{"enable",    DBGCMD_T_ENABLE},
			{"help",      DBGCMD_T_HELP},
			{"quit",      DBGCMD_T_QUIT}
		};

		/* Help message text */
		const ::std::string HELP =
				"LLCCEP debugger commands help.\n"
				"addbp $name $path $line -- create a breakpoint at the file on the line with the name\n"
				"delbp $name             -- delete the breakpoint with the name\n"
				"listbp                  -- list all breakpoints, created so far\n"
				"run                     -- continue/start execution\n"
				"regs                    -- dump registers\n"
				"ram $addr, $length      -- dump given amount of memory cells from the given one\n"
				"regwrite $id, $value    -- write the value to the register with given id\n"
				"ramwrite $addr, $value  -- write the value to the given memory cell\n"
				"call                    -- dump call stack\n"
				"stack                   -- dump stack\n"
				"disable $name           -- disable breakpoint with given name\n"
				"enable $name            -- enable breakpoint with given name\n"
				"help                    -- show help(this text)\n"
				"quit                    -- quit debugger\n";

		/* Debugger command */
		struct debuggerCommand {
			debuggerCommand_t type;
			::std::vector<::std::string> args;
		};

	public:
		debugger();
		~debugger();

		void attachCore(debugCore *core);
		void runDebugProcess();

	private:
		void executeUserCommand();
		bool runCommand(debuggerCommand cmd);
		bool validateCommand(debuggerCommand cmd);
		bool OK() const;

		bool quited;
		debugCore *attachedCore;
	};
}

#endif /* LLCCEP_DEBUGGER_DEBUGGER_HPP */
