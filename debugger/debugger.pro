QT += widgets multimedia

INCLUDEPATH += ./../include/
CONFIG += gnu++14 release

QMAKE_CXXFLAGS_DEBUG += -Wall -g -O0
QMAKE_CXXFLAGS_RELEASE += -Wall -Ofast \
                          -march=native \
                          -funroll-loops \

SOURCES = ../emulator/window/window.cpp \
          ../emulator/window/renderer/renderer.cpp \
          ../emulator/softcore/softcore.cpp \
          ../emulator/mm/mm.cpp \
          ../emulator/signal/signal.cpp \
          ../lib/STDExtras.cpp \
          ../lib/command-line.cpp \
	  ../lib/codeReader.cpp \
	  main.cpp \
          ../emulator/messageBox/messageBox.cpp \
          debugCore/debugCore.cpp \
          ../common/def/def_inst.cpp \
          ../common/def/def_cond.cpp \
          debugger/debugger.cpp

HEADERS = ../emulator/window/window.hpp \
          ../emulator/window/renderer/renderer.hpp \
          ../emulator/softcore/softcore.hpp \
          ../emulator/softcore/fp.hpp \
          ../emulator/mm/mm.hpp \
          ../emulator/signal/signal.hpp \
          ../emulator/messageBox/messageBox.hpp \
          debugCore/debugCore.hpp \
          ../common/def/def_inst.hpp \
          ../common/def/def_cond.hpp \
          debugger/debugger.hpp

TARGET  = "LLCCEP Debugger"
