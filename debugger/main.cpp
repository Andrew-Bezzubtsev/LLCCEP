#include <QApplication>

#include <string>
#include <cstdio>
#include <cstring>

#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/command-line.hpp>
#include <LLCCEP/codeReader.hpp>
#include <LLCCEP/convert.hpp>

#include "debugCore/debugCore.hpp"
#include "debugger/debugger.hpp"

#include "../emulator/messageBox/messageBox.hpp"
#include "../emulator/mm/mm.hpp"
#include "../emulator/signal/signal.hpp"

int main(int argn, char **argv)
{
	/* Initialize QT application.
	   Global, as exceptions handlers
	   may produce message boxes */
	QApplication app(argn, argv);

	try {
		/* Handle signals, sent by program by
		   telling about them to user and
		   ending the execution */
		LLCCEP_exec::cAttachSignalsHandler();

		/* Windows, created by the executing program */
		::std::vector<LLCCEP_exec::window *> windows;

		/* Command-line parameters */
		LLCCEP_tools::commandLineParametersParser clpp;
		/* Add 'help' flag */
		clpp.addFlag(LLCCEP_tools::commandLineFlag{{"--help", "-h"},
							    "help", false});
		/* Add debugger help text */
		clpp.setHelpText("LLCCEP debugger help:\n"
				 "-h/--help    | show help(this text)\n"
				 "The single free parameter will be input "
				 "program");
		/* At least one input program */
		clpp.setMaxFreeParams(1);
		/* Parse command-line parameters */
		clpp.parse(argn, argv);
		/* If needed to show help, show it
		   and end the execution */
		if (clpp.getParam("help") == "1" ||
		    !clpp.getFreeParams().size()) {
			/* If there is no program */
			if (!clpp.getFreeParams().size()) {
				/* Tell about it */
				::std::cerr << "No input!" << ::std::endl;
			}

			/* Show help text */
			clpp.showHelp();
			/* Exit with failure */
			return EINVAL;
		}

		/* Debugger core */
		LLCCEP_debugger::debugCore sc;
		/* Memeory manager */
		LLCCEP_exec::memoryManager mm;
		/* Program reader */
		LLCCEP::codeReader cr;
		/* Debugger */
		LLCCEP_debugger::debugger debugger;

		/* Input file, just the given program */
		::std::FILE *in;
		OPEN_FILE(in, clpp.getFreeParams()[0].c_str(), "r");

		/* Read program header */
		cr.initializeInputFile(in);
		cr.readProgramHeader();
		/* If there are no debug symbols in executable */
		if (!cr.forDebugRun()) {
			/* Throw exception about it */
			throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
				"No debug symbols!"));
		}

		/* Initialize debugger core data */
		sc.setMm(&mm);
		sc.setCodeReader(&cr);

		/* Attach debugger's core */
		debugger.attachCore(&sc);

		/* Run program via debugger */
		debugger.runDebugProcess();

		/* Close input */
		::std::fclose(in);
	} catch (LLCCEP::runtime_exception &exc) {
		/* Spawn message box in case of
		   internal exception */
		LLCCEP_exec::messageBox("Program was interrupted by an exception",
					exc.msg(),
					QMessageBox::Close,
					QMessageBox::Close,
					QMessageBox::Critical).spawn();
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "Program was interrupted by an exception:\n"
				 "%s", exc.msg());
	} catch (::std::exception &exc) {
		/* Spawn message box in case of
		   internal exception */
		LLCCEP_exec::messageBox("Program was interrupted by an exception",
					exc.what(),
					QMessageBox::Close,
					QMessageBox::Close,
					QMessageBox::Critical).spawn();
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "Program was interrupted by an exception:\n"
				 "%s", exc.what());
	} catch (::std::string &err) {
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "%s", err.c_str());
	} catch (int64_t id) {
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "Error " int64_t_pf, id);
	} catch (...) {
		/* Dump to console error information and exit */
		QUITE_ERROR(yes, "Unknown exception");
	}

	return 0;
}
