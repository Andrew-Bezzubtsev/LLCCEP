#include <QEventLoop>
#include <QApplication>

#include <string>
#include <algorithm>
#include <sstream>
#include <utility>

#include <LLCCEP/convert.hpp>
#include <LLCCEP/STDExtras.hpp>
#include <LLCCEP/STLExtras.hpp>

#include "debugCore.hpp"

#define DEBUG_CORE_CHECK_BLOCK(cond) DEFAULT_CHECK_BLOCK(cond, this, OK())
#define DEBUG_CORE_OK DEBUG_CORE_CHECK_BLOCK(true)
#define DEBUG_CORE_NOTOK DEBUG_CORE_CHECK_BLOCK(false)

LLCCEP_debugger::debugCore::debugCore():
	softcore(),
	forcedExecution(false),
	gotError(false)
{ }

LLCCEP_debugger::debugCore::~debugCore()
{ }

void LLCCEP_debugger::debugCore::addBreakpoint(::std::string name,
		::std::string fname, size_t lineOf)
{
	DEBUG_CORE_OK;

	auto files = softcore::reader->getProgramData().fnames;
	if (vec_find(files, fname) == files.end()) {
		::std::cerr << "No such file '" << fname
			    << "' to add breakpoint" << ::std::endl;
		return;
	}

	for (const auto &i: breakpoints) {
		if (i.name == name) {
			::std::cerr << "There's already a breakpoint with '"
				    << name << "' name" << ::std::endl;
			return;
		}

		if (i.fname == fname && i.line == lineOf) {
			::std::cerr << "There's already a breakpoint at '"
				    << fname << "' file at" << lineOf
				    << " line" << ::std::endl;
			return;
		}
	}

	LLCCEP_debugger::debugCore::breakpointInfo info{
		    name, fname, lineOf, true
	};

	breakpoints.push_back(info);

	DEBUG_CORE_OK;
}

void LLCCEP_debugger::debugCore::deleteBreakpoint(::std::string name)
{
	DEBUG_CORE_OK;

	for (auto i = breakpoints.begin(); i < breakpoints.end(); i++) {
		if (i->name == name) {
			breakpoints.erase(i);
			return;
		}
	}

	::std::cerr << "No '" << name << "' breakpoint to be deleted"
	            << ::std::endl;

	DEBUG_CORE_OK;
}

#define MARK_BREAKPOINT(name, state) \
{ \
	DEBUG_CORE_OK; \
	\
	for (auto &i: breakpoints) { \
		if (i.name == name) { \
			i.active = state; \
			return; \
		} \
	} \
	\
	::std::cerr << "No '" << name << "' breakpoint to make " << (state?"":"in") \
		    << "active!" << ::std::endl; \
	\
	DEBUG_CORE_OK; \
}

void LLCCEP_debugger::debugCore::activateBreakpoint(::std::string name)
{
	MARK_BREAKPOINT(name, true);
}

void LLCCEP_debugger::debugCore::deactivateBreakpoint(::std::string name)
{
	MARK_BREAKPOINT(name, true);
}

::std::ostream &LLCCEP_debugger::debugCore::regDump(::std::ostream &out) const
{
	DEBUG_CORE_OK;

	/* Tell about registers dump */
	::std::cout << "Registers dump:" << ::std::endl;

	/* Make 8 rows */
	for (size_t i = 0; i < 8; i++) {
		/* Make 4 columns */
		for (size_t j = 0; j < 4; j++) {
			/* Get register id for current
			 * cell
			 */
			size_t regId = 4 * i + j;
			/* Print register id
			 * and its value
			 */
			::std::cout << "&" << regId << ": " 
				    << softcore::regs[regId] << "; ";
		}

		/* Go to a next row */
		::std::cout << ::std::endl;
	}

	DEBUG_CORE_OK;

	return out;
}

::std::ostream &LLCCEP_debugger::debugCore::memDump(::std::ostream &out,
		size_t begin, size_t length)
{
	DEBUG_CORE_OK;

	/* Tell about ram dump */
	::std::cout << "RAM dump:" << ::std::endl;

	/* For all selected cells */
	for (size_t i = 0; i < length; i++) {
		/* Dump cell id and value
		 * stored in it
		 */
		::std::cout << "[" << begin + i << "]: "
			    << (*softcore::mm)[begin + i]
			    << ::std::endl;
	}

	DEBUG_CORE_OK;

	return out;
}

::std::ostream &LLCCEP_debugger::debugCore::breakpointDump(::std::ostream &out) const
{
	DEBUG_CORE_OK;

	/* Dump single breakpoint info */
	auto dumpBreakpoint = [](LLCCEP_debugger::debugCore::breakpointInfo info) {
		::std::cout << "[" << info.name << "]: " 
		            << "'" << info.fname << "' file, "
			    << info.line << " line, "
			    << (info.active?"":"in") << "active"
			    << ::std::endl;
	};

	/* Tell about breakpoints dump */
	::std::cout << "Breakpoints dump:" << ::std::endl;
	/* For all breakpoints declared so far */
	for (const auto &i: breakpoints) {
		/* Dump breakpoint data */
		dumpBreakpoint(i);
	}

	DEBUG_CORE_OK;

	return out;
}

#define DUMP_STACK(out, telling, stack) \
{ \
	DEBUG_CORE_OK; \
	\
	out << telling << " dump:" << ::std::endl; \
	for (size_t i = 0; i < stack.size(); i++) { \
		out << "[" << i << "]: " << stack[i] \
		    << ::std::endl; \
	} \
	\
	DEBUG_CORE_OK; \
}

::std::ostream &LLCCEP_debugger::debugCore::stackDump(::std::ostream &out) const
{
	DUMP_STACK(out, "Stack", softcore::stk);

	return out;
}

::std::ostream &LLCCEP_debugger::debugCore::callStackDump(::std::ostream &out) const
{
	DUMP_STACK(out, "Call stack", softcore::call);

	return out;
}

#undef DUMP_STACK

::std::string LLCCEP_debugger::debugCore::currentCodeInfo() const
{
	/* Result */
	::std::stringstream res;

	/* Get code reader */
	LLCCEP::codeReader *reader = softcore::reader;
	if (!reader) {
		throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
			"Invalid reader for debugCore"));
	}

	/* Filename */
	::std::string fname = reader->getInstructionFile(softcore::pc);
	res << "__________________________________________________" 
	    << ::std::endl << "Disassembly dump of '" << fname << "':" 
	    << ::std::endl;

	size_t line = reader->getInstructionLine(pc);

	res << "==>  " << line << "  |  "
		<< LLCCEP::getInstructionMnemonic(reader->getInstruction(pc)) << ::std::endl;

	return res.str();
}

void LLCCEP_debugger::debugCore::executeProgram()
{
	DEBUG_CORE_OK;

	continueExecution();

	DEBUG_CORE_OK;
}

void LLCCEP_debugger::debugCore::continueExecution()
{
	DEBUG_CORE_OK;

	if (gotError) {
		gotError = false;
		::std::cerr << "Got an error, restarting program" << ::std::endl;
		softcore::pc = softcore::reader->getProgramData().main_id;
	}

	auto breakpointReached = [this]() {
		::std::string fname = softcore::reader->getInstructionFile(softcore::pc);
		size_t line = softcore::reader->getInstructionLine(softcore::pc);

		for (auto i = breakpoints.begin(); i < breakpoints.end(); i++) {
			if (i->fname == fname && i->line == line)
				return i;
		}

		return breakpoints.end();
	};

	QEventLoop ev;
	while (running()) {
		auto bp = breakpointReached();
		if (bp != breakpoints.end() && bp->active && !forcedExecution) {
			forcedExecution = true;
			break;
		} else if (bp != breakpoints.end() && bp->active && forcedExecution) {
			forcedExecution = false;
		}

		try {
			softcore::executeNextInstruction();
		} catch (::LLCCEP::runtime_exception &exc) {
			::std::cerr << "Execution was interrupted by an exception:" << ::std::endl
				    << exc.msg() << ::std::endl;
			gotError = true;
			break;
		} catch (...) {
			throw RUNTIME_EXCEPTION(CONSTRUCT_MSG(
				"Got runtime error while program execution"));
		}

		ev.processEvents(QEventLoop::AllEvents, 100);
	}

	DEBUG_CORE_OK;
}

void LLCCEP_debugger::debugCore::endExecution()
{
	DEBUG_CORE_OK;

	if (softcore::windows.size()) {
		for (const auto &i: windows)
			i->setMayClose(true);
	} else {
		QApplication::quit();
	}

	DEBUG_CORE_OK;
}

bool LLCCEP_debugger::debugCore::running() const
{
	DEBUG_CORE_OK;

	return !softcore::quit;
}

void LLCCEP_debugger::debugCore::regWrite(size_t id, double value)
{
	DEBUG_CORE_OK;

	if (id > 31) {
		::std::cerr << "Can't write anything to &" << id
			    << ", no such register" << ::std::endl;
		return;
	}

	softcore::regs[id] = value;

	DEBUG_CORE_OK;
}

void LLCCEP_debugger::debugCore::memWrite(size_t id, double value)
{
	DEBUG_CORE_OK;

	(*softcore::mm)[id] = value;

	DEBUG_CORE_OK;
}

bool LLCCEP_debugger::debugCore::OK() const
{
	return softcore::OK();
}

#undef DEBUG_CORE_CHECK_BLOCK
#undef DEBUG_CORE_OK
#undef DEBUG_CORE_NOTOK
