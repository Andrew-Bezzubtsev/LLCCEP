#ifndef DEBUGGER_DEBUGCORE_HPP
#define DEBUGGER_DEBUGCORE_HPP

#include <string>
#include <iostream>

#include <LLCCEP/STDExtras.hpp>

#include "./../../emulator/softcore/softcore.hpp"

namespace LLCCEP_debugger {
	class debugCore: public LLCCEP_exec::softcore {
		struct breakpointInfo {
			::std::string name;
			::std::string fname;
			size_t line;
			bool active;
		};

		::std::vector<breakpointInfo> breakpoints;
		/* Mark breakpoint at line as reached */
		bool forcedExecution;
		bool gotError;

	public:
		debugCore();
		~debugCore();

		void addBreakpoint(::std::string name, ::std::string fname, 
		                   size_t lineOf);
		void deleteBreakpoint(::std::string name);

		void activateBreakpoint(::std::string name);
		void deactivateBreakpoint(::std::string name);

		::std::ostream &regDump(::std::ostream &out) const;
		/* Can modify allocated memory, non-const method */
		::std::ostream &memDump(::std::ostream &out, size_t begin,
					size_t length);
		::std::ostream &breakpointDump(::std::ostream &out) const;
		::std::ostream &stackDump(::std::ostream &out) const;
		::std::ostream &callStackDump(::std::ostream &out) const;
		::std::string currentCodeInfo() const;

		void executeProgram() override;
		void continueExecution();
		void endExecution();

		bool running() const;

		void regWrite(size_t id, double value);
		void memWrite(size_t id, double value);

	private:
		bool OK() const override;

		friend class debugger;
	};
}

#endif /* DEBUGGER_DEBUGCORE_HPP */
